const express = require('express');
const path = require('path');
// const favicon = require('serve-favicon');
const logger = require('morgan');
const bodyParser = require('body-parser');
const session = require('express-session');
const cors = require('cors');
const DB = require('./utils/DBConnection');
const global = require('./utils/Config');
const helmet = require('helmet');
const OurDepartmentRoutes = require('./routes/OurDepartmentRoutes');
const PageConfigurationRoutes = require('./routes/PageConfiguration')
const routesChiefMessage = require('./routes/ChiefMessageRoutes');
const routesMostWanted = require('./routes/MostWantedRoutes')
const GlobalRoutes = require('./routes/GlobalConfigurations');
const UserRoutes = require('./routes/UserRoutes');
const MediaRoutes = require('./routes/MediaRoutes');
const EventRoutes = require('./routes/EventRoutes');
const HomeRoutes = require('./routes/HomeRoutes');
const NewsRoutes = require('./routes/NewsRoutes');
const FileComplaint = require('./routes/FileComplaintRoutes');

class Server {

  constructor() {
    this.app = express();
    this.sesion;
    this.DB = new DB();
  }

  appConfig() {
    // this.db.onConnect();
    this.app.use(bodyParser.json());
    this.app.use(cors());
    this.app.use(logger('dev'));

    this.app.use(bodyParser.urlencoded({ 'extended': 'false' }));

    //public files
    this.app.use(express.static(path.join(__dirname, './public')));
    
    // Setting .html as the default template extension
    this.app.set('view engine', 'html');

    // Initializing the ejs template engine
    this.app.engine('html', require('ejs').renderFile);

    // Telling express where it can find the templates
    /*this.app.set('views', (__dirname + '/../../dist'));*/

    //Files 
    
    this.app.use(helmet());
  }

  /* Including app Routes starts*/
  includeRoutes() {
    new OurDepartmentRoutes(this.app).routesConfig();
    
    new PageConfigurationRoutes(this.app).appRoutes();
    //new routesUser(this.app).routesConfig();
    new routesChiefMessage(this.app).appRoutes();
    new routesMostWanted(this.app).appRoutes();
    new GlobalRoutes(this.app).appRoutes();
    new UserRoutes(this.app).appRoutes();
    new MediaRoutes(this.app).appRoutes();
    new FileComplaint(this.app).appRoutes();
    new EventRoutes(this.app).appRoutes();
    new HomeRoutes(this.app).appRoutes();
    new NewsRoutes(this.app).appRoutes();
  }
  /* Including app Routes ends*/

  appExecute() {

    this.appConfig();
    this.includeRoutes();
    this.setCookieParams();
    this.setExceptionRoutes();
    this.setAngularRoutes();
    // this.http.listen(this.port, this.host, () => {
    //   console.log(`Listening on http://${this.host}:${this.port}`);
    // });
  }

  setAngularRoutes(){
    this.app.get('*', function(req, res) {  
    
      res.set('Content-Type', 'text/html')    
         .sendFile(path.join(__dirname, './public/index.html'));    
    });
    
    
  }

  setCookieParams() {
    this.app.set('trust proxy', 1) // trust first proxy 

    this.app.use(session({
      cookieName: 'nodejssession',
      secret: 'eg[isfd-8yF9-7w2315df{}+Ijsli;;to8',
      duration: 30 * 60 * 1000,
      activeDuration: 5 * 60 * 1000,
      // httpOnly: true,
      // secure: true, //security
      // ephemeral: true
      resave: true,
      saveUninitialized: true,
      cookie: { cookieName: 'nodejssession', expires: new Date(Date.now() + 3600000), maxAge: 3600000 }
    }));
  }

  cookieSessionTest() {
    this.app.get('/', function (req, res) {
      this.sesion = req.session;
      req.session.email = 'algo@alo.com'
      console.log(req);

      if (this.sesion.email) {
        res.write(`
              < h1 > Hello `+ this.sesion.email + `</h1 >
              `);
        res.end(`<a href="+">Logout</a>`);
      } else {
        res.write(`< h1 > Please login first.</h1 >`);
        res.end('<a href="+">Login</a>');
      }

      req.session.destroy(function (err) {
        console.log('Session err ' + err)
      })
    })
  }

  setExceptionRoutes() {
    // catch 404 and forward to error handler
    // this.app.use((req, res, next) => {
    //   var err = new Error('Not Found');
    //   err.status = 404;
    //   next(err, req, res);
    // });

    // // error handler
    // this.app.use((err, req, res, next) => {
    //   // set locals, only providing error in development
    //   res.locals.message = err.message;
    //   res.locals.error = req.app.get('env') === 'development' ? err : {};

    //   // render the error page
    //   res.status(err.status || 500);
    //   // res.render('error');
    //   console.log(err)
    //   // console.log(`url -> : ${req.baseUrl} error -> :  ${err}`)
    // });
  }

  verifyConnectionBD() {
    this.DB.getConnection()
      .authenticate()
      .then(() => {
        this.DB.getConnection().import("./models/officer_rank");
        this.DB.getConnection().import("./models/police_officers");

        // this.DB.getConnection().import('./models/ChiefMessageModel')
        console.log(`Connected to ${global.db.db}`);
      })
      .catch(err => {
        console.error('Unable to connect to the database:', err);
      });
  }
  
  appExpress() {
    this.verifyConnectionBD();
    return this.app;
  }

}

const app = new Server();
app.appExecute();

module.exports = app.appExpress();
