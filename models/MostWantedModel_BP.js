'use strict'
const DB = require('../utils/DBConnection');
this.DB = new DB();
// const Alias = this.DB.getConnection().import('./AliasModel');
module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define('MostWanted_beforeproduction', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        mainAlias:{
            type: DataTypes.STRING(150),
            allowNull: true,
        },
        alias:{
            type: DataTypes.STRING(150),
            allowNull: true,
            defaultValue:"[]"
        },
        height: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        weight: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        hairColor: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        eyeColor: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        sex: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        age: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        race: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        nationality: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        photo: {
            type: DataTypes.TEXT,
            allowNull: true,
            defaultValue:"most_wanted.png"
        },
        charge: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        caution: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        reward: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        inProduction: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        updated: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        deleted: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        inEdition: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
    });
    // Model.hasOne(Alias, { foreignKey: 'idMW' })
    // Verify the table exist, if not it will create
    Model.sync();

    return Model;
}
