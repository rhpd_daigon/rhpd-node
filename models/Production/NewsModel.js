'use strict'
const DB = require('../../utils/DBConnection');
this.DB = new DB();
const production = this.DB.getConnection().import('../NewsModel.js');
  
module.exports = (sequelize, DataTypes) => {
    const news = sequelize.define('news', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
        title: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true
        },
        titleUrl: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true
        },
        descriptionLong: {
            type: DataTypes.TEXT,

        },
        descriptionShort:{
            type: DataTypes.STRING(50),
            
        },
        photo:{
            type: DataTypes.TEXT,
            
            defaultValue:'news.png'
        },
        publishDate:{
            type: DataTypes.DATEONLY,
           
        },
        images:{
            type: DataTypes.TEXT,
           
        }
  
    });
    news.belongsTo(production,{as: 'production',foreignKey: 'id_production',onDelete: 'SET NULL'});
    news.sync();
   
    return news;
}

    