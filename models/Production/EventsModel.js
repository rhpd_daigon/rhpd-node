'use strict'
const DB = require('../../utils/DBConnection');
this.DB = new DB();
const production = this.DB.getConnection().import('../EventsModel.js');
  
module.exports = (sequelize, DataTypes) => {
    const event = sequelize.define('event', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
        title: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true
        },
        titleUrl: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true
        },
        descriptionLong: {
            type: DataTypes.TEXT,

        },
        descriptionShort:{
            type: DataTypes.STRING(50),
            
        },
        photo:{
            type: DataTypes.TEXT,
            
            defaultValue:'event.png'
        },
        publishDate:{
            type: DataTypes.DATEONLY,
           
        },
        images:{
            type: DataTypes.TEXT,
           
        }
  
    });
    event.belongsTo(production,{as: 'production',foreignKey: 'id_production',onDelete: 'SET NULL'});
    event.sync();
   
    return event;
}

    