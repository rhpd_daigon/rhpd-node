'use strict'
const DB = require('../../utils/DBConnection');
this.DB = new DB();
const production = this.DB.getConnection().import('../PageConfiguration.js');
module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define('PageConfiguration', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
       //example
       FileAREportEmail: {    
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: ''
    },
    JoinOurReserveEmail: {    
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: ''
    },
    ComendEmail: {    
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: ''
    },
    ComplainEmail: {    
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: ''
    },
    MainEmail: {    
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: ''
    },
    
    Phone: {    
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: ''
    },
    PhoneFirearm: {    
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: ''
    },
    PhoneCrime: {    
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: ''
    }
    });
   // Verify the table exist, if not it will create

   Model.belongsTo(production,{as: 'production',foreignKey: 'id_production',onDelete: 'SET NULL'});
    Model.sync();

    return Model;
}