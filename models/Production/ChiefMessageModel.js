'use strict'
module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define('chief_message', {
       //example
        title: {    
            type: DataTypes.STRING(50)   
        },
        photo: {    
            type: DataTypes.STRING(50),    
        },
        message: {    
            type: DataTypes.TEXT
        },
        signature: {    
            type: DataTypes.STRING(50)
        },
        ImageHome:{
            type: DataTypes.STRING(50)
        },
        email: {    
            type: DataTypes.STRING(50)
            
        },
    });
   // Verify the table exist, if not it will create
    Model.sync();

    return Model;
}

    
