'use strict'
module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define('HeadersPage', {
       //example
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
        title: {    
            type: DataTypes.STRING(50)   
        },
        content: {    
            type: DataTypes.TEXT,
            allowNull: true  
            
        },
        page: {    
            type: DataTypes.STRING(50)
        }
    });
   // Verify the table exist, if not it will create
    Model.sync();

    return Model;
}

    