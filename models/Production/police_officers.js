'use strict'
const config = require('../../utils/Config');
const DB = require('../../utils/DBConnection');
this.DB = new DB();
const rank = this.DB.getConnection().import('../officer_rank.js');
const production = this.DB.getConnection().import('../police_officers.js');
module.exports = (sequelize, DataTypes) => {
    const police_officers = sequelize.define('police_officers', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
        name: {    
            type: DataTypes.TEXT,
            allowNull: false,
           // unique: ''
        },
        // id_rank: {
        //     type: DataTypes.INTEGER,
        //     allowNull: false,
        // },
        photo: {
            type: DataTypes.TEXT,
            allowNull: true,
            defaultValue:"officer.png"
        },
        division:{
            type: DataTypes.TEXT,
            allowNull: false,
        },
        bio:{
            type: DataTypes.TEXT,
            allowNull: true,
            defaultValue: config.lorem
        },
        fallen:{
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        email:{
            type: DataTypes.STRING(50) ,
            allowNull: true,
            defaultValue:"jmedina@wearedaigon.com"
        },
        fallenDate:{
            type: DataTypes.DATEONLY,
            allowNull:true
        },
        category:{
            type: DataTypes.INTEGER,
            allowNull:false
           },
    });

   // Project.hasOne(User, { foreignKey: 'initiator_id' })
   // Verify the table exist, if not it will create
   police_officers.belongsTo(rank,{as: 'rank',foreignKey: 'id_rank',onDelete: 'SET NULL'});
   police_officers.belongsTo(production,{as: 'production',foreignKey: 'id_production',onDelete: 'SET NULL'});
   police_officers.sync();

    return police_officers;
}

    
