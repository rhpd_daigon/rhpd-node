'use strict'
const DB = require('../../utils/DBConnection');
this.DB = new DB();
const production = this.DB.getConnection().import('../MostWantedModel_BP.js');
// const Alias = this.DB.getConnection().import('./AliasModel');
module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define('MostWanted', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        name: {
            type: DataTypes.STRING(50),
          
        },
        mainAlias:{
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        alias:{
            type: DataTypes.STRING(150),
            allowNull: true,
        },
        height: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        weight: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        hairColor: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        eyeColor: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        sex: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        age: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        race: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        nationality: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        photo: {
            type: DataTypes.TEXT,
            allowNull: true,
            defaultValue:"most_wanted.png"
        },
        charge: {
            type: DataTypes.TEXT,
          
        },
        caution: {
            type: DataTypes.TEXT,
          
        },
        reward: {
            type: DataTypes.TEXT,
          
        }
    });
    // Model.hasOne(Alias, { foreignKey: 'idMW' })
    // Verify the table exist, if not it will create
    Model.belongsTo(production,{as: 'production',foreignKey: 'id_production',onDelete: 'SET NULL'});
    Model.sync();

    return Model;
}
