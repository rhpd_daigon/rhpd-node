'use strict'
const DB = require('../../utils/DBConnection');
this.DB = new DB();
const production = this.DB.getConnection().import('../HomeModel.js');
module.exports = (sequelize, DataTypes) => {
    const Home = sequelize.define('HomeModel', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
       //example
        tab1: {    
            type: DataTypes.TEXT,
            allowNull: false,
            default:'{}'
          
        },
        tab2: {    
            type: DataTypes.TEXT,
            allowNull: false,
            default:'{}'
           
        },
        tab3: {    
            type: DataTypes.TEXT,
            allowNull: false,
            default:'{}'
          
        },
        message: {    
            type: DataTypes.TEXT,
            allowNull: false,
            default:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime tempora accusantium, similique dolore doloremque itaque nemo nesciunt mollitia temporibus ipsam a fugit enim, odio, expedita vero esse magnam! Eos, eum'
            
          
        },
    });
   // Verify the table exist, if not it will create
   Home.belongsTo(production,{as: 'production',foreignKey: 'id_production',onDelete: 'SET NULL'});
    Home.sync();

    return Home;
}