'use strict'
const DB = require('../../utils/DBConnection');
this.DB = new DB();
const production = this.DB.getConnection().import('../MediaModel.js');
module.exports = (sequelize, DataTypes) => {
    const media = sequelize.define('media', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
        title: {    
            type: DataTypes.STRING(50),
          
            unique: true
        },
        descriptionLong: {
            type: DataTypes.TEXT,
            defaultValue:"officer.png"
        },
        descriptionShort:{
            type: DataTypes.STRING(50),   
        },
        photo:{
            type: DataTypes.TEXT,
            defaultValue:'media.png'
        },
        videoUrl:{
            type: DataTypes.TEXT,
        },
        publishDate:{
            type: DataTypes.DATEONLY,
            allowNull: false
        },
    });
    
    media.belongsTo(production,{as: 'production',foreignKey: 'id_production',onDelete: 'SET NULL'});
    media.sync();
   
    return media;
}
