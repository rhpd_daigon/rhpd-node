'use strict'
const DB = require('../utils/DBConnection');
this.DB = new DB();
module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define('chief_message_beforeProduction', {
       //example
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true, 
         },
        title: {    
            type: DataTypes.STRING(50)   
        },
        photo: {    
            type: DataTypes.STRING(50),
            default:'chief-default.png' 
        },
        message: {    
            type: DataTypes.TEXT,
            default:'' 
        },
        signature: {    
            type: DataTypes.STRING(50),
            default:'signature.png'
        },
        email: {    
            type: DataTypes.STRING(50),
            unique:true
        },
        inProduction : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        Updated : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        Deleted : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        InEdition: {
            type: DataTypes.ENUM('1','2','3'),
            default:'1'
        },
        ImageHome:{
            type: DataTypes.STRING(50),
            default:'' 
        }
    });
   // Verify the table exist, if not it will create
    Model.sync();

    return Model;
}

    
