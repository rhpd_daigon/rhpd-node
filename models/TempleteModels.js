'use strict'
module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define('modelName', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
       //example
        name: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: 'idEmployee'
        }
    });
   // Verify the table exist, if not it will create
    Model.sync();

    return Model;
}

    


