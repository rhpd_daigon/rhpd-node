'use strict'
module.exports = (sequelize, DataTypes) => {
    const Home = sequelize.define('HomeModel_BeforeProduction', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
       //example
        tab1: {    
            type: DataTypes.TEXT,
            allowNull: false,
            default:'{}'
        },
        tab2: {    
            type: DataTypes.TEXT,
            allowNull: false,
            default:'{}'
        },
        tab3: {    
            type: DataTypes.TEXT,
            allowNull: false,
            default:'{}'
        },
        message: {    
            type: DataTypes.TEXT,
            allowNull: false,
            default:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime tempora accusantium, similique dolore doloremque itaque nemo nesciunt mollitia temporibus ipsam a fugit enim, odio, expedita vero esse magnam! Eos, eum '
        },
        inProduction : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        Updated : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        Deleted : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        InEdition: {
            type: DataTypes.ENUM('1','2','3'),
            default:'1'
        }
    });
   // Verify the table exist, if not it will create
    Home.sync();

    return Home;
}