'use strict'
module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define('HeadersPage_beforeProduction', {
       //example
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
        title: {    
            type: DataTypes.STRING(50)   
        },
        content: {    
            type: DataTypes.TEXT,
            allowNull: true  
            
        },
        page: {    
            type: DataTypes.STRING(50)
        },
        inProduction : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        Updated : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        Deleted : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        InEdition: {
            type: DataTypes.ENUM('1','2','3'),
            default:'1'
        }
    });
   // Verify the table exist, if not it will create
    Model.sync();

    return Model;
}

    