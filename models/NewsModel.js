'use strict'

module.exports = (sequelize, DataTypes) => {
    const event = sequelize.define('news_beforeProduction', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
        title: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true
        },
        titleUrl: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true
        },
        descriptionLong: {
            type: DataTypes.TEXT,
            allowNull: true,

        },
        descriptionShort:{
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        photo:{
            type: DataTypes.TEXT,
            allowNull: true,
            defaultValue:'event.png'
        },
        publishDate:{
            type: DataTypes.DATEONLY,
            allowNull: false
        },
      
        images:{
            type: DataTypes.TEXT,
            allowNull: true
        },
        inProduction : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        Updated : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        Deleted : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        InEdition: {
            type: DataTypes.ENUM('1','2','3'),
            default:'1'
        }
    });
   
    event.sync();
   
    return event;
}

    