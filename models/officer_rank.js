'use strict'


module.exports = (sequelize, DataTypes) => {
    const officer_rank = sequelize.define('officer_rank', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },

        rank: {    
            type: DataTypes.TEXT,
            allowNull: false,
        },
        category:{
            type: DataTypes.BOOLEAN,
            allowNull: false,
        }
    });
   
   // Verify the table exist, if not it will create
   officer_rank.sync();

    return officer_rank;
}

    

