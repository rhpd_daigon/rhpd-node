'use strict'
module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define('PageConfiguration_beforeProduction', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
       //example
        FileAREportEmail: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: ''
        },
        JoinOurReserveEmail: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: ''
        },
        ComendEmail: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: ''
        },
        ComplainEmail: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: ''
        },
        MainEmail: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: ''
        },
        
        Phone: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: ''
        },
        PhoneFirearm: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: ''
        },
        PhoneCrime: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: ''
        },
        inProduction: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        updated: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        deleted: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        inEdition: {
            type: DataTypes.INTEGER,
            allowNull: true,
        }
    });
   // Verify the table exist, if not it will create
    Model.sync();

    return Model;
}