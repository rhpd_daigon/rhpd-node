'use strict'
const config = require('../utils/Config');
const DB = require('../utils/DBConnection');
this.DB = new DB();
const rank = this.DB.getConnection().import('./officer_rank.js');
module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define('users', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
        Email: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique:true
           // unique: ''
        },
        // id_rank: {
        //     type: DataTypes.INTEGER,
        //     allowNull: false,
        // },
        Password: {
            type: DataTypes.STRING(250),
            allowNull: true,
            defaultValue:"officer.png"
        },
        UserName:{
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        Avatar:{
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        Name :{
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        Surname : {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        departamento_id : {
            type: DataTypes.INTEGER,
            allowNull: true,
        }
        
    });

   // Project.hasOne(User, { foreignKey: 'initiator_id' })
   // Verify the table exist, if not it will create
  
   user.sync();

    return user;
}

    
