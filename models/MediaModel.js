'use strict'
module.exports = (sequelize, DataTypes) => {
    const media = sequelize.define('media_beforeProduction', {
       id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
        title: {    
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true
        },
        descriptionLong: {
            type: DataTypes.TEXT,
            allowNull: true,

        },
        descriptionShort:{
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        photo:{
            type: DataTypes.TEXT,
            allowNull: true,
            defaultValue:'media.png'
        },
        videoUrl:{
            type: DataTypes.TEXT,
            allowNull: true
        },
        publishDate:{
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        inProduction : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        Updated : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        Deleted : {
            type: DataTypes.BOOLEAN,
            default:false                     
        },
        InEdition: {
            type: DataTypes.ENUM('1','2','3'),
            default:'1'
        }
    });
    media.sync();
   
    return media;
}

    