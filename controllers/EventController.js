"use strict";
const DB = require("../utils/DBConnection");
const fs = require("fs");
const path = require("path");
const config = require("../utils/Config");
const sequelize = require("sequelize");
const async = require("async-waterfall");
class EventController {
    constructor() {
        this.DB = new DB();
        this.event = this.DB.getConnection().import(
            "../models/EventsModel.js"
        );
        this.eventProduction = this.DB.getConnection().import(
            "../models/Production/EventsModel.js"
        );



    }
    //add 
    create(request, response) {
        try {

            const eventData = request.body;
            this.event
                .create(eventData)
                .then(event => {
                    if (event != null) {
                        return this.responseCorrectStatus(response, event);
                    }
                })
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });

        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    delete(id, response) {
        try {
            this.event.destroy({ where: { id: id } })
                .then(() => {

                })
                .catch(err => {

                })
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }
    //update
    update(request, id, response) {
        try {
            const eventData = request.body;


            this.event.update(eventData, { where: { id: id } })
                .then(() => this.event.findById(id))
                .then((event) => {
                    return this.responseCorrectStatus(response, event);
                })
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getOne(title, response) {
        try {
            this.event.find({ where: { title: title } })
                .then(event => this.responseCorrectStatus(response, event))
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getOneProduction(title, response) {
        try {
            this.eventProduction.find({ where: { titleUrl: title } })
                .then(event => this.responseCorrectStatus(response, event))
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getOneByTitleUrl(title, response) {
        try {
            this.event.find({ where: { titleUrl: title } })
                .then(event => this.responseCorrectStatus(response, event))
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getModelsPagination(page, res) { }

    getAll(response) {
        try {
            this.event.findAll({ where: { Deleted: false }, order: [['publishDate', 'DESC']] })
                .then(event => {
                    this.responseCorrectStatus(response, event)
                })
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getAllProduction(response) {
        try {
            this.eventProduction.findAll({order: [['publishDate', 'DESC']] })
                .then(event => {
                    this.responseCorrectStatus(response, event)
                })
                .catch(err => {
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    photo(request, id, oldImage, response) {
        const params = request.body;
        var file_name = "event.png";
        if (request.files) {
            const file_path = request.files.image.path; //ruta de la imagen
            file_name = path.basename(file_path); //obtener solo el nombre de la imagen

            const ext_split = file_name.split(".");
            const file_ext = ext_split[1]; //obtener extencion de la imagen

            if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                this.event
                    .update({ photo: file_name }, { where: { id: id } })
                    .then(() => {

                        if (oldImage === null || oldImage === file_name) {
                            return this.responseCorrectStatus(response, 'Done');
                        }
                        else {
                            if (oldImage !== "event.png") {
                                const rootPath = path.normalize(`./public/uploads/event/${oldImage}`);
                                fs.exists(rootPath, exists => {
                                    if (exists) {
                                        fs.unlink(rootPath, err => {
                                            if (err) {
                                                console.log(err);
                                            }
                                            else {
                                                console.log('file removed done');
                                                return this.responseCorrectStatus(response, 'Done with fails');
                                            }
                                        });
                                    }
                                    else {
                                        return this.responseCorrectStatus(response, 'Done with fails');
                                    }

                                });
                            }
                            else {
                                return this.responseCorrectStatus(response, 'Done');
                            }
                        }
                    })
                    .catch(err => {
                        console.log(err)
                        return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                    });
            } else {
                return this.responseServerErrorStatus(response, { errorType: "Server", error: "file extension is not valid" });
            }
        } else {
            return this.responseNotFoundStatus(response, { errorType: " not found", error: "image not found" });
        }
    }

    Addphotos(request, id, response) {
        const params = request.body;
        var file_name = "event.png";
        this.event.findById(id)
            .then(event => {
                if (event != null) {
                    if (request.files) {
                        var imagesToAdd = JSON.parse(event.images);
                        if (imagesToAdd === null) {
                            imagesToAdd = [];
                        }
                        request.files[0]
                        const file_path = request.files[0].path; //ruta de la imagen
                        file_name = path.basename(file_path); //obtener solo el nombre de la imagen

                        const ext_split = file_name.split(".");
                        const file_ext = ext_split[1]; //obtener extencion de la imagen

                        if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif" || file_ext == "jpeg") {
                            var image = { image: file_name, id_event: id };
                            imagesToAdd.push(image);
                        } else {
                            // return this.responseServerErrorStatus(response, {
                            //     errorType: "Server",
                            //     error: "file extension is not valid"
                            // });

                            // return;
                        }

                        const images = JSON.stringify(imagesToAdd);
                        let eventP = { images: images, Updated: true };


                        return this.event.update(eventP, { where: { id: id } })



                    } else {
                        return this.responseNotFoundStatus(response, {
                            errorType: "404",
                            error: "No files to upload"
                        });
                    }
                }
            })
            .then(() => this.event.findById(id))
            .then((event) => {
                if (event !== null)
                    return this.responseCorrectStatus(response, event.images);
                else
                    this.responseServerErrorStatus(response, { type: 'BD', err: 'Event not found' })
            })
            .catch(err => {
                console.log(err)
                this.responseServerErrorStatus(response, { type: 'BD', err: err })
            })

    }

    deleteImages(id, name, response) {
        this.event.findById(id)
            .then(events => {
                if (events != null) {

                    var images = JSON.parse(events.images);
                    const index = images.findIndex(i => i.image === name);
                    if (index > - 1) {
                        images.splice(index, 1);
                        const newImages = JSON.stringify(images);
                        let eventP = { images: newImages, Updated: true };
                        const rootPath = path.normalize(`./public/uploads/event/${name}`);
                        fs.exists(rootPath, exists => {
                            if (exists) {
                                fs.unlink(rootPath, err => {
                                    if (err) {
                                        console.log(err);
                                    }
                                    else {
                                        console.log('file removed done');
                                        return this.event.update(eventP, { where: { id: id } });
                                    }
                                });
                            }
                            else {
                                return this.responseCorrectStatus(response, 'Done with fails');
                            }

                        });

                    }

                }
                else {
                    return this.responseNotFoundStatus(response, 'Image not found');
                }
            })
            .then(() => this.event.findById(id))
            .then((event) => {
                if (event !== null)
                    return this.responseCorrectStatus(response, event.images);
                else
                    this.responseServerErrorStatus(response, { type: 'BD', err: 'event not found' })
            })
            .catch(err => {
                console.log(err)
                this.responseServerErrorStatus(response, { type: 'BD', err: err })
            });
    }

    UpdateImages(request, id, oldImage, response) {
        const params = request.body;
        var file_name = "event.png";
        this.event.findById(id)
            .then(events => {
                if (events != null) {
                    if (request.files) {
                        var imagesToAdd = JSON.parse(events.images);
                        if (imagesToAdd === null) {
                            imagesToAdd = [];
                        }
                        request.files[0]
                        const file_path = request.files[0].path; //ruta de la imagen
                        file_name = path.basename(file_path); //obtener solo el nombre de la imagen

                        const ext_split = file_name.split(".");
                        const file_ext = ext_split[1]; //obtener extencion de la imagen

                        if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif" || file_ext == "jpeg") {
                            const index = imagesToAdd.findIndex(i => i.image === oldImage);

                            var image = { image: file_name, id_event: id };
                            imagesToAdd[index] = image;
                        } else {
                            // return this.responseServerErrorStatus(response, {
                            //     errorType: "Server",
                            //     error: "file extension is not valid"
                            // });
                        }
                        const images = JSON.stringify(imagesToAdd);
                        let eventP = { images: images, Updated: true };
                        const rootPath = path.normalize(`./public/uploads/event/${oldImage}`);
                        fs.exists(rootPath, exists => {
                            if (exists) {
                                fs.unlink(rootPath, err => {
                                    if (err) {
                                        console.log(err);
                                    }
                                    else {
                                        console.log('file removed done');
                                        return this.event.update(eventP, { where: { id: id } })
                                    }
                                });
                            }
                            else {
                                return this.responseCorrectStatus(response, 'Done with fails');
                            }

                        });

                    } else {
                        return this.responseNotFoundStatus(response, { errorType: "404", error: "No files to upload" });
                    }
                }
            })
            .then(() => this.event.findById(id))
            .then((event) => {
                if (event !== null)
                    return this.responseCorrectStatus(response, event.images);
                else
                    this.responseServerErrorStatus(response, { type: 'BD', err: 'event not found' })
            })
            .catch(err => {
                console.log(err)
                this.responseServerErrorStatus(response, { type: 'BD', err: err })
            });
    }

    getPhoto(file, response) {
        let pathfile;
        if (file !== undefined) pathfile = `./public/uploads/event/${file}`;
        fs.exists(pathfile, exists => {
            if (exists) response.sendFile(path.resolve(pathfile));
            else
                response.sendFile(path.resolve(`./public/uploads/event/event.png`));
        });
    }

    responseCorrectStatus(response, params) {
        return response.status(200).json(params);
    }

    responseServerErrorStatus(response, error) {
        return response.status(500).json(error);
    }

    responseNotFoundStatus(response, error) {
        return response.status(400).json(error);
    }

    Production(req, res) {
        this.DB.Connection.transaction().then(t => {
            try {

                Promise.all([
                    this.event.findAll({ where: { inProduction: true, Updated: false, Deleted: false } }, { transaction: t }),
                    this.eventProduction.findAll({}, { transaction: t }),
                ])
                    .then(result => {

                        let eventProduction = [];
                        const event = result[0];
                        const eventProductionvalues = result[1];
                        //Buscar los datos repetidos para dejar solo los que se van a insertar
                        for (let i = 0; i < eventProductionvalues.length; i++) {
                            const index = event.findIndex(o => o.id === eventProductionvalues[i].id);
                            event.splice(index, 1);
                        }
                        if (event.length > 0) {
                            event.forEach(event => {
                                //buscar los datos en produccion para no insertar datos repetidos
                                let eventP = {};
                                eventP.id = event.id;
                                eventP.title = event.title;
                                eventP.titleUrl = event.titleUrl;
                                eventP.photo = event.photo;
                                eventP.descriptionLong = event.descriptionLong;
                                eventP.descriptionShort = event.descriptionShort;
                                eventP.videoUrl = event.videoUrl;
                                eventP.publishDate = event.publishDate;
                                eventP.images = event.images;
                                eventP.id_production = event.id;
                                eventProduction.push(eventP);
                            });
                        }

                        return this.eventProduction.bulkCreate(eventProduction);


                    })
                    .then(() => this.event.findAll({ where: { inProduction: true, Updated: true, } }, { transaction: t }))
                    .then(events => {
                        if (events.length === 0) {
                            return new Promise(function (resolve, reject) {
                                resolve(events);
                            });
                        }
                        //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
                        var ids = [];
                        for (let i = 0; i < events.length; i++) {
                            const event = events[i];
                            ids.push(event.id);
                        }
                        return this.eventProduction.destroy({ where: { id_production: ids } })
                    })
                    .then(() => this.event.findAll({ where: { inProduction: true, Updated: true } }, { transaction: t }))
                    .then((events) => {
                        if (events.length === 0) {
                            return new Promise(function (resolve, reject) {
                                resolve(events);
                            });
                        }
                        let eventProduction = [];
                        if (events.length > 0) {
                            events.forEach(event => {
                                //buscar los datos en produccion para no insertar datos repetidos
                                let eventP = {};
                                eventP.id = event.id;
                                eventP.title = event.title;
                                eventP.titleUrl = event.titleUrl;
                                eventP.photo = event.photo;
                                eventP.descriptionLong = event.descriptionLong;
                                eventP.descriptionShort = event.descriptionShort;
                                eventP.videoUrl = event.videoUrl;
                                eventP.publishDate = event.publishDate;
                                eventP.images = event.images;
                                eventP.id_production = event.id;
                                eventProduction.push(eventP);
                            });
                        }

                        return this.eventProduction.bulkCreate(eventProduction);
                    })
                    .then(() => this.event.update({ Updated: false }, { where: { Updated: true } }))
                    .then(() => this.event.findAll({ where: { inProduction: true, Deleted: true, } }))
                    .then(events => {
                        if (events === null) {
                            return new Promise(function (resolve, reject) {
                                resolve(events);
                            });
                        }
                        //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
                        var ids = [];
                        for (let i = 0; i < events.length; i++) {
                            const officer = events[i];
                            ids.push(officer.id);
                        }
                        return this.eventProduction.destroy({ where: { id_production: ids } })


                    })
                    .then(() => {
                        t.commit();
                        return this.responseCorrectStatus(res, 'The site has been published correctly');
                    })
                    .catch((err) => {
                        console.log('Error ==========================>' + err);
                        t.rollback();
                        return this.responseServerErrorStatus(res, err);
                    });

            } catch (e) {
                console.log(e);
                return this.responseServerErrorStatus(res, 'Error');
            }
        });
    }

}

module.exports = EventController;
