'use strict';
const DB = require('../utils/DBConnection');
const fs = require('fs');
const path = require('path');
class TempleteController {

    constructor() {
        this.DB = new DB();
        this.ModelName = this.DB.getConnection().import('../models/TempleteModels');
        this.modelData = {
            id: '',
            name: ''
        };
    }

    createModel(request, response) {
        try {
            const messageDataRequest = request.body;

            this.modelData.message = messageDataRequest.message;
            this.modelData.userFrom = messageDataRequest.userFrom;

            this.ModelName.create(this.modelData)
                .then(model => {
                    if (!model) {
                        this.responseCorrectStatus(response, model);
                    }
                })
                .catch(err => {
                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
            this.resetMessage();
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }

    }

    deleteModel(id, response) {
        try {
            this.ModelName.findByIdAndRemove(id)
                .then(model => {
                    if (!model) {
                        this.responseCorrectStatus(response, model);
                    }
                })
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }


    }

    updateModel(request, id, response) {
        try {
            const ModelDataRequest = request.body;
            this.ModelName.findByIdAndUpdate(id, messageDataRequest)
                .then(model => this.responseCorrectStatus(response, model))
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }

    }

    getModel(id, response) {
        try {
            this.ModelName.findById(id)
                .then(model => this.responseCorrectStatus(response, model))
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }
    }



    getModelsPagination(page, res) {
        try {
            var limit = 10;   // number of records per page
            var offset = 0;

            this.ModelName.findAndCountAll()
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return this.ModelName.findAll({
                        limit: limit,
                        offset: offset,
                        $sort: { id: 1 }
                    })

                })
                .then(result => {

                    return this.reshelpobject(200, { users: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error })
        }


    }

    getAllUsers(res) {

        try {
            this.ModelName.findAll({})
                .then(result => {

                    return this.responseCorrectStatus(response, { modelName: result });
                })
                .catch(err => {
                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }
    }

    login(req, res) {

    }

    resetData() {
        this.userData = {
            id: '',
            name: '',
        }
    }

    UploadImageToSomeone(request, id, response) {
        const params = request.body;
        const file_name = 'ideas.png';
        const file_path = req.files.image.path;
        if (request.files) {
            file_name = path.basename(file_path);

            const ext_split = file_name.split('\.');
            const file_ext = ext_split[1];

            if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif') {


                this.ModelName.update({ image: file_name }, { where: { id: id } })
                    .then(() => User.findById(_id))
                    .then(user => {

                        if (user !== null) {
                            this.responseCorrectStatus(response, { user, image: file_name })

                            if (params.oldimage !== 'userDefault.png') {
                                const rootPath = path.normalize(`${__dirname}/uploads/${id}/users`);
                                const oldImage = rootPath + '\\' + params.oldimage;
                                fs.unlink(oldImage, (err => {
                                    if (err) {
                                        return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
                                    }
                                }));
                            }
                            return;
                        }
                        else {
                            return this.responseNotFoundStatus(response, { errorType: "Not found", error: error });
                        }


                    })
                    .catch(error => {
                        return this.responseServerErrorStatus(response, { errorType: "BD", error: error });
                    });
            } else {
                return this.responseServerErrorStatus(response, { errorType: "Server", error: 'Extension del archivo no valida' });
                fs.unlink(file_path, (err => {
                    if (err) {
                        console.log(err)
                    }
                }));
                return;
            }

        } else {

            return this.responseNotFoundStatus(response, { errorType: "No has subido una imagen", error: error });
        }
    }

    getImageFile(request, response) {
        const imageFile = req.params.imageFile;
        const pathfile = './uploads/users/' + imageFile;

        fs.exists(pathfile, (exists) => {
            if (exists)
                response.sendFile(path.resolve(pathfile));
            else
            return this.responseNotFoundStatus(response, { errorType: "No has subido una imagen", error: error });
        });


    }

    responseCorrectStatus(response, params) {
        return response.status(200).json(params);
    }

    responseServerErrorStatus(response, error) {
        return response.status(500).json(error);
    }

    responseNotFoundStatus(response, error) {
        return response.status(400).json(error);
    }

}

module.exports = TempleteController;