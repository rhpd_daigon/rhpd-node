'use strict';
const DB = require('../utils/DBConnection');
const fs = require('fs');
const path = require('path');
class TempleteController {

    constructor() {
        this.config = require('../utils/Config');
        this.DB = new DB();
        this.ChiefModel = this.DB.Connection.import('../models/ChiefMessageModel');
        this.ChiefModelProduction = this.DB.Connection.import('../models/Production/ChiefMessageModel');
        this.modelData = {
            title: '',
            photo: '',
            message: '',
            signature: ''
        };
    }

    updateData(request, response) {
        try {

            const Data = request.body;
            const id = 1;
            this.ChiefModel.update(Data, { where: { id: id } })
                .then(() => this.ChiefModel.findAll({ where: { id: id } }))
                .then(ChiefModel => this.responseCorrectStatus(response, { ChiefModel: ChiefModel }))

                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });


        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }

    }

    getData(response) {
        try {
            if (true) {
                this.ChiefModel.findOne({})
                    .then(model => { return this.responseCorrectStatus(response, model) })
                    .catch(err => {
                        console.log(err);

                        return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                    });
            }
            else {
                return this.responseNotFoundStatus(response, { errorType: "Server", error: "Id not Found" });
            }

        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }
    }

    getDataProduction(response) {
        try {
            if (true) {
                this.ChiefModelProduction.findById(1)
                    .then(model => { return this.responseCorrectStatus(response, model) })
                    .catch(err => {
                        console.log(err);

                        return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                    });
            }
            else {
                return this.responseNotFoundStatus(response, { errorType: "Server", error: "Id not Found" });
            }

        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }
    }

    resetData() {
        this.modelData = {
            title: '',
            photo: '',
            message: '',
            signature: ''
        };
    }

    UploadPhoto(request, id, action, oldImage, response) {
        const params = request.body;
        const idMessage = 1;
        let file_name = 'chief-default.png';

        const file_path = request.files.image.path; //ruta de la imagen
        if (request.files) {
            file_name = path.basename(file_path);//obtener solo el nombre de la imagen

            const ext_split = file_name.split('\.');
            const file_ext = ext_split[1];//obtener extencion de la imagen 

            if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif' || file_ext == 'jpeg') {
                if (action === '1') {

                    this.ChiefModel.update({ photo: file_name }, { where: { id: idMessage } })
                        .then(() => {
                            if (oldImage === null || oldImage === file_name) {
                                return this.responseCorrectStatus(response, 'Done');
                            }
                            else {
                                if (oldImage !== "chief-default.png") {
                                    const rootPath = path.normalize(`./public/uploads/chief_message/${oldImage}`);
                                    fs.exists(rootPath, exists => {
                                        if (exists) {
                                            fs.unlink(rootPath, err => {
                                                if (err) {
                                                    console.log(err);
                                                }
                                                else {
                                                    console.log('file removed done');
                                                    return this.responseCorrectStatus(response, 'Done with fails');
                                                }
                                            });
                                        }
                                        else {
                                            return this.responseCorrectStatus(response, 'Done with fails');
                                        }

                                    });
                                }
                                else {
                                    return this.responseCorrectStatus(response, 'Done');
                                }

                            }
                        })
                        .catch(error => {
                            return this.responseServerErrorStatus(response, { errorType: "BD", error: error });
                        });

                }
                else if (action === '2') {

                    this.ChiefModel.update({ signature: file_name }, { where: { id: idMessage } })
                        .then(() => {
                            if (oldImage === null || oldImage === file_name) {
                                return this.responseCorrectStatus(response, 'Done');
                            }
                            else {
                                if (oldImage !== "signature.png") {
                                    const rootPath = path.normalize(`./public/uploads/chief_message/${oldImage}`);
                                    fs.exists(rootPath, exists => {
                                        if (exists) {
                                            fs.unlink(rootPath, err => {
                                                if (err) {
                                                    console.log(err);
                                                }
                                                else {
                                                    console.log('file removed done');
                                                    return this.responseCorrectStatus(response, 'Done with fails');
                                                }
                                            });
                                        }
                                        else {
                                            return this.responseCorrectStatus(response, 'Done with fails');
                                        }

                                    });
                                }
                                else {
                                    return this.responseCorrectStatus(response, 'Done');
                                }
                            }
                        })
                        .catch(error => {
                            return this.responseServerErrorStatus(response, { errorType: "BD", error: error });
                        });

                }
                else if (action ==='3') {

                    this.ChiefModel.update({ ImageHome: file_name }, { where: { id: idMessage } })
                        .then(() => {


                            if (oldImage === null || oldImage === file_name) {
                                return this.responseCorrectStatus(response, 'Done');
                            }
                            else {
                                if (oldImage !== "home.png") {
                                    const rootPath = path.normalize(`./public/uploads/chief_message/${oldImage}`);
                                    fs.exists(rootPath, exists => {
                                        if (exists) {
                                            fs.unlink(rootPath, err => {
                                                if (err) {
                                                    console.log(err);
                                                }
                                                else {
                                                    console.log('file removed done');
                                                    return this.responseCorrectStatus(response, 'Done with fails');
                                                }
                                            });
                                        }
                                        else {
                                            return this.responseCorrectStatus(response, 'Done with fails');
                                        }

                                    });
                                }
                                else {
                                    return this.responseCorrectStatus(response, 'Done');
                                }
                            }
                        })
                        .catch(error => {
                            return this.responseServerErrorStatus(response, { errorType: "BD", error: error });
                        });

                }

            } else {
                return this.responseServerErrorStatus(response, { errorType: "Server", error: 'file extension is not valid' });

            }

        } else {

            return this.responseNotFoundStatus(response, { errorType: " not found", error: "image not found" });
        }
    }

    getPhoto(request, file, response) {
        try {

            const pathfile = './public/uploads/chief_message/' + file;

            fs.exists(pathfile, (exists) => {
                if (exists)
                    response.sendFile(path.resolve(pathfile));
                else
                    return response.sendFile(path.resolve('./public/uploads/chief_message/home.png'));
            },
                err => {
                    console.log(err)
                });

        } catch (error) {
            console.log(error)
        }



    }

    production(res) {
        this.DB.Connection.transaction().then(t => {
            try {

                this.ChiefModel.findAll({ where: { inProduction: true, Updated: false, Deleted: false } }, { transaction: t })
                    .then(ChiefModel => {
                        if (ChiefModel === null) {
                            return new Promise(function (resolve, reject) {
                                resolve(ChiefModel);
                            });
                        }
                        //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar

                        return this.ChiefModelProduction.destroy({ where: { id: 1 } })
                    })
                    .then(() => this.ChiefModel.findOne({ where: { inProduction: true, Updated: false, Deleted: false } }, { transaction: t }))
                    .then(ChiefModel => {


                        //buscar los datos en produccion para no insertar datos repetidos
                        let ChiefModelP = {};
                       
                        ChiefModelP.title = ChiefModel.title;
                        ChiefModelP.photo = ChiefModel.photo;
                        ChiefModelP.message = ChiefModel.message;
                        ChiefModelP.signature = ChiefModel.signature;
                        ChiefModelP.ImageHome = ChiefModel.ImageHome;
                        ChiefModelP.email = ChiefModel.email;


                        return  this.ChiefModelProduction.findOrCreate({
                            where: { id: 1 },
                            defaults: ChiefModelP
                          });
                    })
                    .then(() => {
                        t.commit();
                        return this.responseCorrectStatus(res, 'The site has been published correctly');
                    })
                    .catch((err) => {
                        console.log(err);
                        t.rollback();
                        return this.responseServerErrorStatus(res, err);
                    });

            } catch (e) {
                console.log(e);
                return this.responseServerErrorStatus(res, 'Error');
            }
        });
    }

    responseCorrectStatus(response, params) {
        return response.status(200).json(params);
    }

    responseServerErrorStatus(response, error) {
        return response.status(500).json(error);
    }

    responseNotFoundStatus(response, error) {
        return response.status(400).json(error);
    }

}

module.exports = TempleteController;