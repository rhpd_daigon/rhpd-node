"use strict";
const DB = require("../utils/DBConnection");
const fs = require("fs");
const path = require("path");
const bcrypt = require("bcrypt-nodejs");
var nodemailer = require("nodemailer");

class UserController {
  constructor() {
    this.jwt = require('../middlewares/jwt');
    this.config = require("../utils/Config");
    this.DB = new DB();
    this.User = this.DB.Connection.import("../models/User.js");

    bcrypt.hash("Admin1234.", null, null, (err, hash) => {
      const user = {
        Email: "jmedina@wearedaigon.com",
        Password: hash,
        UserName: "rhpd",
        Avatar: null,
        Name: "Harrison",
        Surname: "PD"
      };

      this.User.findOrCreate({ where:{id:1}, defaults:user})
      .catch(error => {
        console.log(error);
      });
    });

    bcrypt.hash("Admin123", null, null, (err, hash) => {
      const user = {
        Email: "brittany.thurston@relativityinc.com",
        Password: hash,
        UserName: "rhpd",
        Avatar: null,
        Name: "Harrison",
        Surname: "PD"
      };

      this.User.findOrCreate({ where:{id:2}, defaults:user})
      .catch(error => {
        console.log(error);
      });
    });
  }

  loginWeb(req, res) {
    const userProps = req.body;
    
    const password = userProps.Password;
    const email = userProps.Email;

    this.User.findOne({where: { Email: email.toLowerCase()}})
      .then(user => {
        if (user != null) {
          bcrypt.compare(password, user.Password, (err, check) => {
            if (err) {
              return this.responseServerErrorStatus(res, "Error" + err );
            }

            if (check) {
                user.Password = null;
              this.responseCorrectStatus(res,{token: this.jwt(user),user:user});
            } else {
              this.responseNotFoundStatus(res, "The password is incorrect");
            }
          });
        } else this.responseNotFoundStatus(res, "The user not found");
      })
      .catch(error => {
        this.responseServerErrorStatus(res, error);
      });
  }

  responseCorrectStatus(response, params) {
    return response.status(200).json(params);
  }

  responseServerErrorStatus(response, error) {
    return response.status(500).json(error);
  }

  responseNotFoundStatus(response, error) {
    return response.status(400).json(error);
  }
}

module.exports = UserController;
