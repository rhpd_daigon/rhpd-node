"use strict";
const DB = require("../utils/DBConnection");
const fs = require("fs");
const path = require("path");
const config = require("../utils/Config");
const sequelize = require("sequelize");
const async = require("async-waterfall");
class NewsController {
    constructor() {
        this.DB = new DB();
        this.news = this.DB.getConnection().import(
            "../models/NewsModel.js"
        );
        this.newsProduction = this.DB.getConnection().import(
            "../models/Production/NewsModel.js"
        );



    }
    //add 
    create(request, response) {
        try {

            const newsData = request.body;
            this.news
                .create(newsData)
                .then(news => {
                    if (news != null) {
                        return this.responseCorrectStatus(response, news);
                    }
                })
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });

        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    delete(id, response) {
        try {
            this.news.destroy({ where: { id: id } })
                .then(() => {

                })
                .catch(err => {

                })
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }
    //update
    update(request, id, response) {
        try {
            const newsData = request.body;


            this.news.update(newsData, { where: { id: id } })
                .then(() => this.news.findById(id))
                .then((news) => {
                    return this.responseCorrectStatus(response, news);
                })
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getOne(title, response) {
        try {
            this.news.find({ where: { title: title } })
                .then(news => this.responseCorrectStatus(response, news))
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getOneProduction(title, response) {
        try {
            this.newsProduction.find({ where: { titleUrl: title } })
                .then(news => this.responseCorrectStatus(response, news))
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getOneByTitleUrl(title, response) {
        try {
            this.news.find({ where: { titleUrl: title } })
                .then(news => this.responseCorrectStatus(response, news))
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getModelsPagination(page, res) { }

    getAll(response) {
        try {
            this.news.findAll({ where: { Deleted: false }, order: [['publishDate', 'DESC']] })
                .then(news => {
                    this.responseCorrectStatus(response, news)
                })
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getAllProduction(response) {
        try {
            this.newsProduction.findAll({ order: [['publishDate', 'DESC']] })
                .then(news => {
                    this.responseCorrectStatus(response, news)
                })
                .catch(err => {
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    photo(request, id, oldImage, response) {
        const params = request.body;
        var file_name = "news.png";
        if (request.files) {
            const file_path = request.files.image.path; //ruta de la imagen
            file_name = path.basename(file_path); //obtener solo el nombre de la imagen

            const ext_split = file_name.split(".");
            const file_ext = ext_split[1]; //obtener extencion de la imagen

            if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                this.news
                    .update({ photo: file_name }, { where: { id: id } })
                    .then(() => {

                        if (oldImage === null || oldImage === file_name) {
                            return this.responseCorrectStatus(response, 'Done');
                        }
                        else {
                            if (oldImage !== "news.png") {
                                const rootPath = path.normalize(`./public/uploads/news/${oldImage}`);
                                fs.exists(rootPath, exists => {
                                    if (exists) {
                                        fs.unlink(rootPath, err => {
                                            if (err) {
                                                console.log(err);
                                            }
                                            else {
                                                console.log('file removed done');
                                                return this.responseCorrectStatus(response, 'Done with fails');
                                            }
                                        });
                                    }
                                    else {
                                        return this.responseCorrectStatus(response, 'Done with fails');
                                    }

                                });
                            }
                            else {
                                return this.responseCorrectStatus(response, 'Done');
                            }
                        }
                    })
                    .catch(err => {
                        console.log(err)
                        return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                    });
            } else {
                return this.responseServerErrorStatus(response, { errorType: "Server", error: "file extension is not valid" });
            }
        } else {
            return this.responseNotFoundStatus(response, { errorType: " not found", error: "image not found" });
        }
    }

    Addphotos(request, id, response) {
        const params = request.body;
        var file_name = "news.png";
        this.news.findById(id)
            .then(news => {
                if (news != null) {
                    if (request.files) {
                        var imagesToAdd = JSON.parse(news.images);
                        if (imagesToAdd === null) {
                            imagesToAdd = [];
                        }
                        request.files[0]
                        const file_path = request.files[0].path; //ruta de la imagen
                        file_name = path.basename(file_path); //obtener solo el nombre de la imagen

                        const ext_split = file_name.split(".");
                        const file_ext = ext_split[1]; //obtener extencion de la imagen

                        if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif" || file_ext == "jpeg") {
                            var image = { image: file_name, id_news: id };
                            imagesToAdd.push(image);
                        } else {
                            // return this.responseServerErrorStatus(response, {
                            //     errorType: "Server",
                            //     error: "file extension is not valid"
                            // });
                        }
                        const images = JSON.stringify(imagesToAdd);
                        let newsP = { images: images, Updated: true };
                        return this.news.update(newsP, { where: { id: id } })
                    } else {
                        return this.responseNotFoundStatus(response, { errorType: "404", error: "No files to upload" });
                    }
                }
            })
            .then(() => this.news.findById(id))
            .then((news) => {
                if (news !== null)
                    return this.responseCorrectStatus(response, news.images);
                else
                    this.responseServerErrorStatus(response, { type: 'BD', err: 'news not found' })
            })
            .catch(err => {
                console.log(err)
                this.responseServerErrorStatus(response, { type: 'BD', err: err })
            });

    }

    deleteImages(id, name, response) {
        this.news.findById(id)
            .then(news => {
                if (news != null) {

                    var images = JSON.parse(news.images);
                    const index = images.findIndex(i => i.image === name);
                    if (index > - 1) {
                        images.splice(index, 1);
                        const newImages = JSON.stringify(images);
                        let newsP = { images: newImages, Updated: true };
                        const rootPath = path.normalize(`./public/uploads/news/${name}`);
                        fs.exists(rootPath, exists => {
                            if (exists) {
                                fs.unlink(rootPath, err => {
                                    if (err) {
                                        console.log(err);
                                    }
                                    else {
                                        console.log('file removed done');
                                        return this.news.update(newsP, { where: { id: id } });
                                    }
                                });
                            }
                            else {
                                return this.responseCorrectStatus(response, 'Done with fails');
                            }

                        });

                    }

                }
                else {
                    return this.responseNotFoundStatus(response, 'Image not found');
                }
            })
            .then(() => this.news.findById(id))
            .then((news) => {
                if (news !== null)
                    return this.responseCorrectStatus(response, news.images);
                else
                    this.responseServerErrorStatus(response, { type: 'BD', err: 'news not found' })
            })
            .catch(err => {
                console.log(err)
                this.responseServerErrorStatus(response, { type: 'BD', err: err })
            });
    }

    UpdateImages(request, id, oldImage, response) {
        const params = request.body;
        var file_name = "news.png";
        this.news.findById(id)
            .then(news => {
                if (news != null) {
                    if (request.files) {
                        var imagesToAdd = JSON.parse(news.images);
                        if (imagesToAdd === null) {
                            imagesToAdd = [];
                        }
                        request.files[0]
                        const file_path = request.files[0].path; //ruta de la imagen
                        file_name = path.basename(file_path); //obtener solo el nombre de la imagen

                        const ext_split = file_name.split(".");
                        const file_ext = ext_split[1]; //obtener extencion de la imagen

                        if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif" || file_ext == "jpeg") {
                            const index = imagesToAdd.findIndex(i => i.image === oldImage);

                            var image = { image: file_name, id_news: id };
                            imagesToAdd[index] = image;
                        } else {
                            // return this.responseServerErrorStatus(response, {
                            //     errorType: "Server",
                            //     error: "file extension is not valid"
                            // });
                        }
                        const images = JSON.stringify(imagesToAdd);
                        let newsP = { images: images, Updated: true };
                        const rootPath = path.normalize(`./public/uploads/news/${oldImage}`);
                        fs.exists(rootPath, exists => {
                            if (exists) {
                                fs.unlink(rootPath, err => {
                                    if (err) {
                                        console.log(err);
                                    }
                                    else {
                                        console.log('file removed done');
                                        return this.news.update(newsP, { where: { id: id } })
                                    }
                                });
                            }
                            else {
                                return this.responseCorrectStatus(response, 'Done with fails');
                            }

                        });

                    } else {
                        return this.responseNotFoundStatus(response, { errorType: "404", error: "No files to upload" });
                    }
                }
            })
            .then(() => this.news.findById(id))
            .then((news) => {
                if (news !== null)
                    return this.responseCorrectStatus(response, news.images);
                else
                    this.responseServerErrorStatus(response, { type: 'BD', err: 'news not found' })
            })
            .catch(err => {
                console.log(err)
                this.responseServerErrorStatus(response, { type: 'BD', err: err })
            });
    }

    getPhoto(file, response) {
        let pathfile;
        if (file !== undefined) pathfile = `./public/uploads/news/${file}`;
        fs.exists(pathfile, exists => {
            if (exists) response.sendFile(path.resolve(pathfile));
            else
                response.sendFile(path.resolve(`./public/uploads/news/news.png`));
        });
    }

    responseCorrectStatus(response, params) {
        return response.status(200).json(params);
    }

    responseServerErrorStatus(response, error) {
        return response.status(500).json(error);
    }

    responseNotFoundStatus(response, error) {
        return response.status(400).json(error);
    }

    Production(req, res) {
        this.DB.Connection.transaction().then(t => {
            try {

                Promise.all([
                    this.news.findAll({ where: { inProduction: true, Updated: false, Deleted: false } }, { transaction: t }),
                    this.newsProduction.findAll({}, { transaction: t }),
                ])
                    .then(result => {

                        let newsProduction = [];
                        const news = result[0];
                        const newsProductionvalues = result[1];
                        //Buscar los datos repetidos para dejar solo los que se van a insertar
                        for (let i = 0; i < newsProductionvalues.length; i++) {
                            const index = news.findIndex(o => o.id === newsProductionvalues[i].id);
                            news.splice(index, 1);
                        }
                        if (news.length > 0) {
                            news.forEach(news => {
                                //buscar los datos en produccion para no insertar datos repetidos
                                let newsP = {};
                                newsP.id = news.id;
                                newsP.title = news.title;
                                newsP.titleUrl = news.titleUrl;
                                newsP.photo = news.photo;
                                newsP.descriptionLong = news.descriptionLong;
                                newsP.descriptionShort = news.descriptionShort;
                                newsP.videoUrl = news.videoUrl;
                                newsP.publishDate = news.publishDate;
                                newsP.images = news.images;
                                newsP.id_production = news.id;
                                newsProduction.push(newsP);
                            });
                        }

                        return this.newsProduction.bulkCreate(newsProduction);


                    })
                    .then(() => this.news.findAll({ where: { inProduction: true, Updated: true, } }, { transaction: t }))
                    .then(news => {
                        if (news.length === 0) {
                            return new Promise(function (resolve, reject) {
                                resolve(news);
                            });
                        }
                        //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
                        var ids = [];
                        for (let i = 0; i < news.length; i++) {
                            const newsData = news[i];
                            ids.push(newsData.id);
                        }
                        return this.newsProduction.destroy({ where: { id_production: ids } })
                    })
                    .then(() => this.news.findAll({ where: { inProduction: true, Updated: true } }, { transaction: t }))
                    .then((news) => {
                        if (news.length === 0) {
                            return new Promise(function (resolve, reject) {
                                resolve(news);
                            });
                        }
                        let newsProduction = [];
                        if (news.length > 0) {
                            news.forEach(news => {
                                //buscar los datos en produccion para no insertar datos repetidos
                                let newsP = {};
                                newsP.id = news.id;
                                newsP.title = news.title;
                                newsP.titleUrl = news.titleUrl;
                                newsP.photo = news.photo;
                                newsP.descriptionLong = news.descriptionLong;
                                newsP.descriptionShort = news.descriptionShort;
                                newsP.videoUrl = news.videoUrl;
                                newsP.publishDate = news.publishDate;
                                newsP.images = news.images;
                                newsP.id_production = news.id;
                                newsProduction.push(newsP);
                            });
                        }

                        return this.newsProduction.bulkCreate(newsProduction);
                    })
                    .then(() => this.news.update({ Updated: false }, { where: { Updated: true } }))
                    .then(() => this.news.findAll({ where: { inProduction: true, Deleted: true, } }))
                    .then(news => {
                        if (news === null) {
                            return new Promise(function (resolve, reject) {
                                resolve(news);
                            });
                        }
                        //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
                        var ids = [];
                        for (let i = 0; i < news.length; i++) {
                            const newsData = news[i];
                            ids.push(newsData.id);
                        }
                        return this.newsProduction.destroy({ where: { id_production: ids } })


                    })
                    .then(() => {
                        t.commit();
                        return this.responseCorrectStatus(res, 'The site has been published correctly');
                    })
                    .catch((err) => {
                        console.log('Error ==========================>' + err);
                        t.rollback();
                        return this.responseServerErrorStatus(res, err);
                    });

            } catch (e) {
                console.log(e);
                return this.responseServerErrorStatus(res, 'Error');
            }
        });
    }

}

module.exports = NewsController;
