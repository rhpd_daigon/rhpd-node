'use strict';
const DB = require('../utils/DBConnection');
const fs = require('fs');
const path = require('path');
class PageConfigurationController {

    constructor() {
        this.DB = new DB();
        this.PageConfiguration = this.DB.getConnection().import('../models/PageConfiguration');
        this.PageConfigurationProduction = this.DB.getConnection().import('../models/Production/PageConfiguration')
        this.modelData = {
            id: '',
            name: ''
        };
    }



    updateModel(request, response) {
        try {
            const ModelDataRequest = request.body;
            this.PageConfiguration.update(ModelDataRequest,{where:{id:1}})
                .then(model => this.responseCorrectStatus(response, model))
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }

    }

    get(response) {
        try {
            this.PageConfiguration.findById(1)
                .then(model =>{
                    
                    return  this.responseCorrectStatus(response, model)
                })
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }
    }

    getProduction(response) {
        try {
            this.PageConfigurationProduction.findById(1)
                .then(model => this.responseCorrectStatus(response, model))
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }
    }

    
    production(res) {
        this.DB.Connection.transaction().then(t => {
            try {

                this.PageConfiguration.findOne({ where: { inProduction: true } }, { transaction: t })
                    .then(PageConfiguration => {
                        if (PageConfiguration === null) {
                            return new Promise(function (resolve, reject) {
                                resolve(PageConfiguration);
                            });
                        }
                        //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar

                        return this.PageConfigurationProduction.destroy({ where: { id: 1 } })
                    })
                    .then(() => this.PageConfiguration.findOne({ where: { inProduction: true, Updated: false, Deleted: false } }, { transaction: t }))
                    .then(PageConfiguration => {


                        //buscar los datos en produccion para no insertar datos repetidos
                        let PageConfigurationP = {};
                       
                        PageConfigurationP.FileAREportEmail = PageConfiguration.FileAREportEmail;
                        PageConfigurationP.JoinOurReserveEmail = PageConfiguration.JoinOurReserveEmail;
                        PageConfigurationP.ComendEmail = PageConfiguration.ComendEmail;
                        PageConfigurationP.ComplainEmail = PageConfiguration.ComplainEmail;
                        PageConfigurationP.MainEmail = PageConfiguration.MainEmail;
                        PageConfigurationP.Phone = PageConfiguration.Phone;
                        PageConfigurationP.PhoneFirearm = PageConfiguration.PhoneFirearm;
                        PageConfigurationP.PhoneCrime = PageConfiguration.PhoneCrime;
         


                        return  this.PageConfigurationProduction.findOrCreate({
                            where: { id: 1 },
                            defaults: PageConfigurationP
                          });
                    })
                    .then(() => {
                        t.commit();
                        return this.responseCorrectStatus(res, 'The site has been published correctly');
                    })
                    .catch((err) => {
                        console.log(err);
                        t.rollback();
                        return this.responseServerErrorStatus(res, err);
                    });

            } catch (e) {
                console.log(e);
                return this.responseServerErrorStatus(res, 'Error');
            }
        });
    }

    responseCorrectStatus(response, params) {
        return response.status(200).json(params);
    }

    responseServerErrorStatus(response, error) {
        return response.status(500).json(error);
    }

    responseNotFoundStatus(response, error) {
        return response.status(400).json(error);
    }

}

module.exports = PageConfigurationController;