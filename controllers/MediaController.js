"use strict";
const DB = require("../utils/DBConnection");
const fs = require("fs");
const path = require("path");
const config = require("../utils/Config");
const sequelize = require("sequelize");
const async = require("async-waterfall");
class DepartmentController {
    constructor() {
        this.DB = new DB();
        this.media = this.DB.getConnection().import(
            "../models/MediaModel.js"
        );
        this.mediaProduction = this.DB.getConnection().import(
            "../models/Production/MediaModel.js"
        );
    }
    //add 
    create(request, response) {
        try {

            const mediaData = request.body;
            this.media
                .create(mediaData)
                .then(media => {
                    if (media != null) {
                        return this.responseCorrectStatus(response, media);
                    }
                })
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });

        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    delete(id, response) {
        try {
            this.media.destroy({ where: { id: id } })
                .then(() => {

                })
                .catch(err => {

                })
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }
    //update
    update(request, id, response) {
        try {
            const mediaData = request.body;
            this.media.update(mediaData, { where: { id: id } })
                .then(() => this.media.findById(id))
                .then((media) => {
                    return this.responseCorrectStatus(response, media);
                })
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getOne(title, response) {
        try {
            this.media.find({ where: { title: title } })
                .then(media => this.responseCorrectStatus(response, media))
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getOneByTitle(title, response) {
        try {
            this.media.find({ where: { title: title } })
                .then(media => this.responseCorrectStatus(response, media))
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getModelsPagination(page, res) { }

    getAll(response) {
        try {
            this.media.findAll({ where: { Deleted: false }, order: [['publishDate', 'DESC']] })
                .then(media => {
                    this.responseCorrectStatus(response, media)
                })
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    getAllProduction(response) {
        try {
            this.mediaProduction.findAll({order: [['publishDate', 'DESC']] })
                .then(media => {
                    this.responseCorrectStatus(response, media)
                })
                .catch(err => {
                    return this.responseServerErrorStatus(response, {
                        errorType: "BD",
                        error: err
                    });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, {
                errorType: "Server",
                error: error
            });
        }
    }

    photo(request, id, oldImage, response) {
        const params = request.body;
        var file_name = "media.png";
        if (request.files) {
            const file_path = request.files.image.path; //ruta de la imagen
            file_name = path.basename(file_path); //obtener solo el nombre de la imagen

            const ext_split = file_name.split(".");
            const file_ext = ext_split[1]; //obtener extencion de la imagen

            if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                this.media
                    .update({ photo: file_name }, { where: { id: id } })
                    .then(() => {

                        if (oldImage === null || oldImage === file_name) {
                            return this.responseCorrectStatus(response, 'Done');
                        }
                        else {
                            if (oldImage !== "media.png") {
                                const rootPath = path.normalize(`./public/uploads/media/${oldImage}`);
                                fs.exists(rootPath, exists => {
                                    if (exists) {
                                        fs.unlink(rootPath, err => {
                                            if (err) {
                                                console.log(err);
                                            }
                                            else {
                                                console.log('file removed done');
                                                return this.responseCorrectStatus(response, 'Done with fails');
                                            }
                                        });
                                    }
                                    else {
                                        return this.responseCorrectStatus(response, 'Done with fails');
                                    }

                                });
                            }
                            else {
                                return this.responseCorrectStatus(response, 'Done');
                            }
                        }
                    })
                    .catch(err => {
                        console.log(err)
                        return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                    });
            } else {
                return this.responseServerErrorStatus(response, { errorType: "Server", error: "file extension is not valid" });
            }
        } else {
            return this.responseNotFoundStatus(response, { errorType: " not found", error: "image not found" });
        }
    }


    getPhoto(file, response) {
        let pathfile;
        if (file !== undefined) pathfile = `./public/uploads/media/${file}`;
        fs.exists(pathfile, exists => {
            if (exists) response.sendFile(path.resolve(pathfile));
            else
                response.sendFile(path.resolve(`./public/uploads/media/media.png`));
        });
    }

    responseCorrectStatus(response, params) {
        return response.status(200).json(params);
    }

    responseServerErrorStatus(response, error) {
        return response.status(500).json(error);
    }

    responseNotFoundStatus(response, error) {
        return response.status(400).json(error);
    }

    Production(req, res) {
        this.DB.Connection.transaction().then(t => {
            try {

                Promise.all([
                    this.media.findAll({ where: { inProduction: true, Updated: false, Deleted: false } }, { transaction: t }),
                    this.mediaProduction.findAll({}, { transaction: t }),
                ])
                    .then(result => {

                        let mediaProductiondata = [];
                        const media = result[0];
                        const mediaProductionvalues = result[1];
                        //Buscar los datos repetidos para dejar solo los que se van a insertar
                        for (let i = 0; i < mediaProductionvalues.length; i++) {
                            const index = media.findIndex(o => o.id === mediaProductionvalues[i].id);
                            media.splice(index, 1);
                        }
                        if (media.length > 0) {
                            media.forEach(mediadata => {
                                //buscar los datos en produccion para no insertar datos repetidos
                                let mediaP = {};
                                mediaP.id = mediadata.id;
                                mediaP.title = mediadata.title;
                                mediaP.photo = mediadata.photo;
                                mediaP.descriptionLong = mediadata.descriptionLong;
                                mediaP.descriptionShort = mediadata.descriptionShort;
                                mediaP.videoUrl = mediadata.videoUrl;
                                mediaP.publishDate = mediadata.publishDate;
                                mediaP.id_production = mediaP.id;
                                mediaProductiondata.push(mediaP);
                            });
                        }

                        return this.mediaProduction.bulkCreate(mediaProductiondata);


                    })
                    .then(() => this.media.findAll({ where: { inProduction: true, Updated: true, } }, { transaction: t }))
                    .then(media => {
                        if (media.length === 0) {
                            return new Promise(function (resolve, reject) {
                                resolve(media);
                            });
                        }
                        //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
                        var ids = [];
                        for (let i = 0; i < media.length; i++) {
                            const mediaData = media[i];
                            ids.push(mediaData.id);
                        }
                        return this.mediaProduction.destroy({ where: { id_production: ids } })
                    })
                    .then(() => this.media.findAll({ where: { inProduction: true, Updated: true } }, { transaction: t }))
                    .then((media) => {
                        if (media.length === 0) {
                            return new Promise(function (resolve, reject) {
                                resolve(media);
                            });
                        }
                        let mediaProductiondata = [];
                        if (media.length > 0) {
                            media.forEach(mediadata => {
                                //buscar los datos en produccion para no insertar datos repetidos
                                let mediaP = {};
                                mediaP.id = mediadata.id;
                                mediaP.title = mediadata.title;
                                mediaP.photo = mediadata.photo;
                                mediaP.descriptionLong = mediadata.descriptionLong;
                                mediaP.descriptionShort = mediadata.descriptionShort;
                                mediaP.videoUrl = mediadata.videoUrl;
                                mediaP.publishDate = mediadata.publishDate;
                                mediaP.id_production = mediaP.id;
                                mediaProductiondata.push(mediaP);
                            });
                        }

                        return this.mediaProduction.bulkCreate(mediaProductiondata);
                    })
                    .then(() => this.media.update({ Updated: false }, { where: { Updated: true } }))
                    .then(() => this.media.findAll({ where: { inProduction: true, Deleted: true, } }))
                    .then(media => {
                        if (media === null) {
                            return new Promise(function (resolve, reject) {
                                resolve(media);
                            });
                        }
                        //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
                        var ids = [];
                        for (let i = 0; i < media.length; i++) {
                            const mediaData = media[i];
                            ids.push(mediaData.id);
                        }
                        return this.mediaProduction.destroy({ where: { id_production: ids } })


                    })
                    .then(() => {
                        t.commit();
                        return this.responseCorrectStatus(res, 'The site has been published correctly');
                    })
                    .catch((err) => {
                        console.log( err);
                        t.rollback();
                        return this.responseServerErrorStatus(res, err);
                    });

            } catch (e) {
                console.log(e);
                return this.responseServerErrorStatus(res, 'Error');
            }
        });
    }

}

module.exports = DepartmentController;
