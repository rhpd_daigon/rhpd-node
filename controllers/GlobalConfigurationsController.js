"use strict";
const DB = require("../utils/DBConnection");
const fs = require("fs");
const path = require("path");
const config = require("../utils/Config");
const request = require('request');
const xml2js = require('xml2js');
const Sequelize = require('sequelize');

class GlobalConfigurationsController {
  constructor() {
    this.DB = new DB();
    this.police_officers = this.DB.getConnection().import("../models/police_officers.js");
    this.videos = this.DB.getConnection().import("../models/MediaModel.js");
    this.events = this.DB.getConnection().import("../models/EventsModel.js");
    this.news = this.DB.getConnection().import("../models/NewsModel.js");
    this.police_officersProduction = this.DB.getConnection().import("../models/Production/police_officers.js");
    this.videosProduction = this.DB.getConnection().import("../models/Production/MediaModel.js");
    this.eventsProduction = this.DB.getConnection().import("../models/Production/EventsModel.js");
    this.newsProduction = this.DB.getConnection().import("../models/Production/NewsModel.js");

    this.ranks = this.DB.getConnection().import("../models/officer_rank.js");


    this.populateQuery = [{ model: this.ranks, as: "rank" }];
    this.Op = Sequelize.Op;
    this.headers = this.DB.getConnection().import("../models/HeadersModel.js");
    this.headersProduction = this.DB.getConnection().import("../models/Production/HeadersModel.js");

    this.headers.findOrCreate({
      where: { id: 1 },
      defaults: {
        title: "OurDepartment", page: "OurDepartment", content: "Officers Harrison PD", inProduction: true, Updated: false, Deleted: false, inEdition: '1'
      }
    });
    this.headers.findOrCreate({
      where: { id: 2 },
      defaults: {
        title: "FallenOfficers",
        page: "FallenOfficers",
        content: config.lorem,
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });
    this.headers.findOrCreate({
      where: { id: 3 },
      defaults: {
        title: "MostWanted",
        page: "MostWanted",
        content: config.lorem,
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });
    this.headers.findOrCreate({
      where: { id: 4 },
      defaults: {
        title: "fileReport",
        page: "fileReport",
        content: 'Please provide real information, our system will store your ip for security and spam reasons.',
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });

    this.headers.findOrCreate({
      where: { id: 5 },
      defaults: {
        title: "joinReserveTeam",
        page: "joinReserveTeam",
        content: 'Please provide real information, our system will store your ip for security and spam reasons.',
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });

    this.headers.findOrCreate({
      where: { id: 6 },
      defaults: {
        title: "joinReserveTeamImage",
        page: "joinReserveTeamImage",
        content: 'header.png',
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });

    this.headers.findOrCreate({
      where: { id: 7 },
      defaults: {
        title: "fileCommend",
        page: "fileCommend",
        content: 'Welcome to Harrison Police Department Community Online Reporting Service. Using this service allows you to submit select police reports at your convenience.',
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });

    this.headers.findOrCreate({
      where: { id: 8 },
      defaults: {
        title: "fileComplaint",
        page: "fileComplaint",
        content: 'Welcome to Harrison Police Department Community Online Reporting Service. Using this service allows you to submit select police reports at your convenience.',
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });

    this.headers.findOrCreate({
      where: { id: 9 },
      defaults: {
        title: "SubmitTip",
        page: "SubmitTip",
        content: `Tips can be submitted anonymously or with contact and/or location information. It is up to you to choose how they submit. The system allows secure two way communication that allows you to remain anonymous.

        QuikTip is a secure two way communication component that allows you to provide tips and information to the user agencies.`,
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });

    this.headers.findOrCreate({
      where: { id: 10 },
      defaults: {
        title: "Home",
        page: "Home",
        content: config.lorem,
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });
    this.headersProduction.findOrCreate({
      where: { id: 1 },
      defaults: {
        title: "OurDepartment",
        page: "OurDepartment",
        content: "Officers Harrison PD"
      }
    });
    this.headersProduction.findOrCreate({
      where: { id: 2 },
      defaults: {
        title: "FallenOfficers",
        page: "FallenOfficers",
        content: config.lorem
      }
    });
    this.headersProduction.findOrCreate({
      where: { id: 3 },
      defaults: {
        title: "MostWanted",
        page: "MostWanted",
        content: config.lorem
      }
    });
    this.headersProduction.findOrCreate({
      where: { id: 4 },
      defaults: {
        title: "fileReport",
        page: "fileReport",
        content: 'Please provide real information, our system will store your ip for security and spam reasons.',
      }
    });
    this.headersProduction.findOrCreate({
      where: { id: 5 },
      defaults: {
        title: "joinReserveTeam",
        page: "joinReserveTeam",
        content: 'Please provide real information, our system will store your ip for security and spam reasons.'
      }
    });

    this.headersProduction.findOrCreate({
      where: { id: 6 },
      defaults: {
        title: "joinReserveTeamImage",
        page: "joinReserveTeamImage",
        content: 'header.png'
      }
    });

    this.headersProduction.findOrCreate({
      where: { id: 7 },
      defaults: {
        title: "fileCommend",
        page: "fileCommend",
        content: 'Welcome to Harrison Police Department Community Online Reporting Service. Using this service allows you to submit select police reports at your convenience.',
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });

    this.headersProduction.findOrCreate({
      where: { id: 8 },
      defaults: {
        title: "fileComplaint",
        page: "fileComplaint",
        content: 'Welcome to Harrison Police Department Community Online Reporting Service. Using this service allows you to submit select police reports at your convenience.',
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });

    this.headersProduction.findOrCreate({
      where: { id: 9 },
      defaults: {
        title: "SubmitTip",
        page: "SubmitTip",
        content: `Tips can be submitted anonymously or with contact and/or location information. It is up to you to choose how they submit. The system allows secure two way communication that allows you to remain anonymous.

        QuikTip is a secure two way communication component that allows you to provide tips and information to the user agencies.`,
        inProduction: true,
        Updated: false,
        Deleted: false,
        inEdition: '1'
      }
    });

    this.headersProduction.findOrCreate({
      where: { id: 10 },
      defaults: {
        title: "Home",
        page: "Home",
        content: config.lorem,
        
      }
    });

  }


  getTitle(page, response) {
    this.headers
      .find({ where: { page: page } })
      .then(titles => this.responseCorrectStatus(response, titles))
      .catch(err => {
        console.warn(err);
        this.responseServerErrorStatus(response, { errorType: "BD", error: err });
      });
  }

  getTitleProduction(page, response) {
    this.headersProduction
      .find({ where: { page: page } })
      .then(titles => this.responseCorrectStatus(response, titles))
      .catch(err => {
        console.warn(err);
        this.responseServerErrorStatus(response, { errorType: "BD", error: err });
      });
  }

  UpdateTitle(req, page, response) {

    const data = req.body;
    this.headers
      .update(data, { where: { page: page } })
      .then(titles => this.headers.find({ where: { page: page } }))
      .then(titles => this.responseCorrectStatus(response, titles))
      .catch(err => {
        console.warn(err);
        this.responseServerErrorStatus(response, {
          errorType: "BD",
          error: err
        });
      });
  }

  getDispatches(response) {

    request('http://www.myrps.com/downloads/CustomDownloads/Harrison/HarrisonDispatchList.xml', (err, res, body) => {
      if (err) { this.responseServerErrorStatus(response, 'Dispatches not found') }


      xml2js.parseString(body, (err, result) => {
        if (err) { this.responseServerErrorStatus(response, 'Dispatches not found') }
        var dispatches = result.DispatchExportList.DispatchExportItem;
        this.responseCorrectStatus(response, dispatches);
      });
    });

  }

  sercher(text, response) {
    Promise.all([
      this.police_officers
        .findAll({
          where: { fallen: false, [this.Op.or]: [{ 'name': { [this.Op.regexp]: text } }, { 'division': { [this.Op.regexp]: text } }, { 'email': { [this.Op.regexp]: text } }] }
          , include: this.populateQuery
        }),
      this.police_officers.
        findAll({
          where: { fallen: true, [this.Op.or]: [{ 'name': { [this.Op.regexp]: text } }, { 'division': { [this.Op.regexp]: text } }, { 'email': { [this.Op.regexp]: text } }] }
          , include: this.populateQuery
        }),
      this.events.
        findAll({ where: { [this.Op.or]: [{ 'title': { [this.Op.regexp]: text } }, { 'descriptionShort': { [this.Op.regexp]: text } }, { 'descriptionLong': { [this.Op.regexp]: text } }] } }),
      this.news.
        findAll({ where: { [this.Op.or]: [{ 'title': { [this.Op.regexp]: text } }, { 'descriptionShort': { [this.Op.regexp]: text } }, { 'descriptionLong': { [this.Op.regexp]: text } }] } }),
      this.videos.
        findAll({ where: { [this.Op.or]: [{ 'title': { [this.Op.regexp]: text } }, { 'descriptionShort': { [this.Op.regexp]: text } }, { 'descriptionLong': { [this.Op.regexp]: text } }] } }),
    ])
      .then(result => {
        let arrayElements = { officers: [], fallen: [], news: [], events: [], videos: [] }
        arrayElements.officers = result[0];
        arrayElements.fallen = result[1];
        arrayElements.events = result[2];
        arrayElements.news = result[3];
        arrayElements.videos = result[4];
        this.responseCorrectStatus(response, arrayElements);
      })
      .catch(err => {
        console.log(err);
        this.responseServerErrorStatus(response, err);
      })
  }

  sercherProduction(text, response) {
    Promise.all([
      this.police_officersProduction
        .findAll({
          where: { fallen: false, [this.Op.or]: [{ 'name': { [this.Op.regexp]: text } }, { 'division': { [this.Op.regexp]: text } }, { 'email': { [this.Op.regexp]: text } }] }
          , include: this.populateQuery
        }),
      this.police_officersProduction
        .findAll({
          where: { fallen: true, [this.Op.or]: [{ 'name': { [this.Op.regexp]: text } }, { 'division': { [this.Op.regexp]: text } }, { 'email': { [this.Op.regexp]: text } }] }
          , include: this.populateQuery
        }),
      this.eventsProduction
        .findAll({ where: { [this.Op.or]: [{ 'title': { [this.Op.regexp]: text } }, { 'descriptionShort': { [this.Op.regexp]: text } }, { 'descriptionLong': { [this.Op.regexp]: text } }] } }),
      this.newsProduction
        .findAll({ where: { [this.Op.or]: [{ 'title': { [this.Op.regexp]: text } }, { 'descriptionShort': { [this.Op.regexp]: text } }, { 'descriptionLong': { [this.Op.regexp]: text } }] } }),
      this.videosProduction
        .findAll({ where: { [this.Op.or]: [{ 'title': { [this.Op.regexp]: text } }, { 'descriptionShort': { [this.Op.regexp]: text } }, { 'descriptionLong': { [this.Op.regexp]: text } }] } }),
    ])
      .then(result => {
        let arrayElements = { officers: [], fallen: [], news: [], events: [], videos: [] }
        arrayElements.officers = result[0];
        arrayElements.fallen = result[1];
        arrayElements.events = result[2];
        arrayElements.news = result[3];
        arrayElements.videos = result[4];
        this.responseCorrectStatus(response, arrayElements);
      })
      .catch(err => {
        console.log(err);
        this.responseServerErrorStatus(response, err);
      })
  }

  productionHeaders(res) {
    this.DB.Connection.transaction().then(t => {
      try {

        this.headers.findAll({ where: { inProduction: true, Updated: false, Deleted: false } }, { transaction: t })
          .then(headers => {
            if (headers === null) {
              return new Promise(function (resolve, reject) {
                resolve(headers);
              });
            }
            //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
            var ids = [];
            for (let i = 0; i < headers.length; i++) {
              const headersData = headers[i];
              ids.push(headersData.id);
            }
            return this.headersProduction.destroy({ where: { id: ids } })
          })
          .then(() => this.headers.findAll({ where: { inProduction: true, Updated: false, Deleted: false } }, { transaction: t }))
          .then(headers => {

            let headersProductiondata = [];
            const headersData = headers[0];

            if (headers.length > 0) {
              headers.forEach(headersdata => {
                //buscar los datos en produccion para no insertar datos repetidos
                let headersP = {};
                headersP.id = headersdata.id;
                headersP.title = headersdata.title;
                headersP.content = headersdata.content;
                headersP.page = headersdata.page;
                headersProductiondata.push(headersP);
              });
            }
            return this.headersProduction.bulkCreate(headersProductiondata);
          })
          .then(() => {
            t.commit();
            return this.responseCorrectStatus(res, 'The site has been published correctly');
          })
          .catch((err) => {
            console.log(err);
            t.rollback();
            return this.responseServerErrorStatus(res, err);
          });

      } catch (e) {
        console.log(e);
        return this.responseServerErrorStatus(res, 'Error');
      }
    });
  }

  photo(request, id, oldImage, response) {
    const params = request.body;
    var file_name = "header.png";
    if (request.files) {
      const file_path = request.files.image.path; //ruta de la imagen
      file_name = path.basename(file_path); //obtener solo el nombre de la imagen

      const ext_split = file_name.split(".");
      const file_ext = ext_split[1]; //obtener extencion de la imagen

      if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
        this.headers
          .update({ content: file_name }, { where: { page: id } })
          .then(() => {

            if (oldImage === null || oldImage === file_name) {
              return this.responseCorrectStatus(response, 'Done');
            }
            else {
              if (oldImage !== "header.png") {
                const rootPath = path.normalize(`./public/uploads/headers/${oldImage}`);
                fs.exists(rootPath, exists => {
                  if (exists) {
                    fs.unlink(rootPath, err => {
                      if (err) {
                        console.log(err);
                      }
                      else {
                        console.log('file removed done');
                        return this.responseCorrectStatus(response, 'Done with fails');
                      }
                    });
                  }
                  else {
                    return this.responseCorrectStatus(response, 'Done with fails');
                  }

                });
              }
              else {
                return this.responseCorrectStatus(response, 'Done');
              }
            }
          })
          .catch(err => {
            console.log(err)
            return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
          });
      } else {
        return this.responseServerErrorStatus(response, { errorType: "Server", error: "file extension is not valid" });
      }
    } else {
      return this.responseNotFoundStatus(response, { errorType: " not found", error: "image not found" });
    }
  }

  getPhoto(file, response) {
    let pathfile;
    if (file !== undefined) pathfile = `./public/uploads/headers/${file}`;
    fs.exists(pathfile, exists => {
      if (exists) response.sendFile(path.resolve(pathfile));
      else
        response.sendFile(path.resolve(`./public/uploads/headers/header.png`));
    });
  }

  responseCorrectStatus(response, params) {
    return response.status(200).json(params);
  }

  responseServerErrorStatus(response, error) {
    return response.status(500).json(error);
  }

  responseNotFoundStatus(response, error) {
    return response.status(400).json(error);
  }
}

module.exports = GlobalConfigurationsController;