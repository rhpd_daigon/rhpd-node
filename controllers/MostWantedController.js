'use strict';
const DB = require('../utils/DBConnection');
const fs = require('fs');
const path = require('path');
class MostWantedController {

    constructor() {
        this.DB = new DB();
        this.MostWanted = this.DB.getConnection().import('../models/MostWantedModel_BP');
        this.mostwantedProduction = this.DB.getConnection().import('../models/Production/MostWantedModel')
        this.wanted = {
            name: '',
            mainAlias: '',
            height: '',
            weight: '',
            hairColor: '',
            eyeColor: '',
            sex: '',
            age: '',
            race: '',
            nationality: '',
            photo: '',
            charge: '',
            caution: '',
            reward: '',
            inProduction: '',
            updated: '',
            deleted: '',
            inEdition: ''
        };
        this.modelData = {
            id: '',
            alias: '',
            idMW: ''
        };
    }

    create(request, response) {
        try {
            const data = request.body;
            
            this.MostWanted.create(data)
                .then(wanted => {
                    return this.responseCorrectStatus(response, wanted)
                })

                .catch(err => {
                    console.log(err)

                })

        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }

    }

    deleteModel(request, id, response) {
        try {

            this.MostWanted.destroy({ where: { id: id } })
                .then(() => this.MostWanted.findAll({ where: { id: id } }))
                .then(mostwanted => {
                    return this.responseCorrectStatus(response, { MostWanted: mostwanted });
                })
                .catch(err => {
                    console.log(err);
                })
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }


    }

    updateModel(request, id, response) {
        try {
            const data = request.body;
            const wanted = data[0];

            this.MostWanted.update(wanted, { where: { id: id } })

                .then(() => this.MostWanted.findById(id))
                .then(wanted => {
                    return this.responseCorrectStatus(response, { Mostwanted: wanted })
                })
                .catch(err => {
                    console.log(err);
                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                })


        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }

    }

    getMostWanted(id, response) {
        try {
            this.MostWanted.findById(id)
                .then(wanted => {
                    return this.responseCorrectStatus(response, wanted)
                })
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }
    }

    getModelsPagination(page, response) {
        try {
            var limit = 10;   // number of records per page
            var offset = 0;

            this.ModelName.findAndCountAll()
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return this.ModelName.findAll({
                        limit: limit,
                        offset: offset,
                        $sort: { id: 1 }
                    })

                })
                .then(result => {

                    return this.reshelpobject(200, { users: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error })
        }


    }

    getAllWanted(response) {

        try {
            this.MostWanted.findAll({where:{Deleted:false}})
                .then(result => {

                    return this.responseCorrectStatus(response, { MostWanted: result });
                })
                .catch(err => {
                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }
    }

    getAllWantedProduction(response) {

        try {
            this.mostwantedProduction.findAll({})
                .then(result => {

                    return this.responseCorrectStatus(response, { MostWanted: result });
                })
                .catch(err => {
                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }
    }

    resetData() {
        this.userData = {
            id: '',
            name: '',
        }
    }

    addImage(request, id, oldImage, response) {
        const params = request.body;
        var file_name = "most_wanted.png";
        if (request.files) {
            const file_path = request.files.image.path; //ruta de la imagen
            file_name = path.basename(file_path); //obtener solo el nombre de la imagen

            const ext_split = file_name.split(".");
            const file_ext = ext_split[1]; //obtener extencion de la imagen

            if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                this.MostWanted
                    .update({ photo: file_name }, { where: { id: id } })
                    .then(() => {

                        if (oldImage === null || oldImage === file_name) {
                            return this.responseCorrectStatus(response, 'Done');
                        }
                        else {
                            if (oldImage !== "most_wanted.png") {
                                const rootPath = path.normalize(`./public/uploads/most_wanted/${oldImage}`);
                                fs.exists(rootPath, exists => {
                                    if (exists) {
                                        fs.unlink(rootPath, err => {
                                            if (err) {
                                                console.log(err);
                                            }
                                            else {
                                                console.log('file removed done');
                                                return this.responseCorrectStatus(response, 'Done with fails');
                                            }
                                        });
                                    }
                                    else {
                                        return this.responseCorrectStatus(response, 'Done with fails');
                                    }

                                });
                            }
                            else {
                                return this.responseCorrectStatus(response, 'Done');
                            }
                        }
                    })
                    .catch(err => {
                        console.log(err)
                        return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                    });
            } else {
                return this.responseServerErrorStatus(response, { errorType: "Server", error: "file extension is not valid" });
            }
        } else {
            return this.responseNotFoundStatus(response, { errorType: " not found", error: "image not found" });
        }
    }


    getImageFile(request, file, response) {
        let pathfile = './public/uploads/most_wanted/' + file;

        if (file !== undefined) pathfile = './public/uploads/most_wanted/' + file;
        fs.exists(pathfile, exists => {
            if (exists) response.sendFile(path.resolve(pathfile));
            else
                response.sendFile(path.resolve('./public/uploads/most_wanted/most_wanted.png'));
        });


    }

    responseCorrectStatus(response, params) {
        return response.status(200).json(params);
    }

    responseServerErrorStatus(response, error) {
        return response.status(500).json(error);
    }

    responseNotFoundStatus(response, error) {
        return response.status(400).json(error);
    }


    Production(req, res) {
        this.DB.Connection.transaction().then(t => {
            try {

                Promise.all([
                    this.MostWanted.findAll({ where: { inProduction: true, Updated: false, Deleted: false } }, { transaction: t }),
                    this.mostwantedProduction.findAll({}, { transaction: t }),
                ])
                    .then(result => {

                        let mostwantedProductiondata = [];
                        const mw = result[0];
                        const mwProductionvalues = result[1];
                        //Buscar los datos repetidos para dejar solo los que se van a insertar
                        for (let i = 0; i < mwProductionvalues.length; i++) {
                            const index = mw.findIndex(o => o.id === mwProductionvalues[i].id);
                            mw.splice(index, 1);
                        }
                        if (mw.length > 0) {
                            mw.forEach(mostwanteddata => {
                                //buscar los datos en produccion para no insertar datos repetidos
                                let mostwantedP = {};
                                mostwantedP.id = mostwanteddata.id;
                                mostwantedP.name = mostwanteddata.name;
                                mostwantedP.mainAlias = mostwanteddata.mainAlias;
                                mostwantedP.alias = mostwanteddata.alias;
                                mostwantedP.height = mostwanteddata.height;
                                mostwantedP.weight = mostwanteddata.weight;
                                mostwantedP.hairColor = mostwanteddata.hairColor;
                                mostwantedP.eyeColor = mostwanteddata.eyeColor;
                                mostwantedP.sex = mostwanteddata.sex;
                                mostwantedP.age = mostwanteddata.age;
                                mostwantedP.race = mostwanteddata.race;
                                mostwantedP.nationality = mostwanteddata.nationality;
                                mostwantedP.photo = mostwanteddata.photo;
                                mostwantedP.charge = mostwanteddata.charge;
                                mostwantedP.caution = mostwanteddata.caution;
                                mostwantedP.reward = mostwanteddata.reward;
                                mostwantedP.id_production = mostwanteddata.id;
                                mostwantedProductiondata.push(mostwantedP);
                            });
                        }

                        return this.mostwantedProduction.bulkCreate(mostwantedProductiondata);


                    })
                    .then(() => this.MostWanted.findAll({ where: { inProduction: true, Updated: true, } }, { transaction: t }))
                    .then(mw => {
                        if (mw.length === 0) {
                            return new Promise(function (resolve, reject) {
                                resolve(mw);
                            });
                        }
                        //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
                        var ids = [];
                        for (let i = 0; i < mw.length; i++) {
                            const mwData = mw[i];
                            ids.push(mwData.id);
                        }
                        return this.mostwantedProduction.destroy({ where: { id_production: ids } })
                    })
                    .then(() => this.MostWanted.findAll({ where: { inProduction: true, Updated: true } }, { transaction: t }))
                    .then((mw) => {
                        if (mw.length === 0) {
                            return new Promise(function (resolve, reject) {
                                resolve(mw);
                            });
                        }
                        let mostwantedProductiondata = [];
                        if (mw.length > 0) {
                            mw.forEach(mostwanteddata => {
                                //buscar los datos en produccion para no insertar datos repetidos
                                let mostwantedP = {};
                                mostwantedP.id = mostwanteddata.id;
                                mostwantedP.name = mostwanteddata.name;
                                mostwantedP.mainAlias = mostwanteddata.mainAlias;
                                mostwantedP.alias = mostwanteddata.alias;
                                mostwantedP.height = mostwanteddata.height;
                                mostwantedP.weight = mostwanteddata.weight;
                                mostwantedP.hairColor = mostwanteddata.hairColor;
                                mostwantedP.eyeColor = mostwanteddata.eyeColor;
                                mostwantedP.sex = mostwanteddata.sex;
                                mostwantedP.age = mostwanteddata.age;
                                mostwantedP.race = mostwanteddata.race;
                                mostwantedP.nationality = mostwanteddata.nationality;
                                mostwantedP.photo = mostwanteddata.photo;
                                mostwantedP.charge = mostwanteddata.charge;
                                mostwantedP.caution = mostwanteddata.caution;
                                mostwantedP.reward = mostwanteddata.reward;
                                mostwantedP.id_production = mostwanteddata.id;
                                mostwantedProductiondata.push(mostwantedP);
                            });
                        }

                        return this.mostwantedProduction.bulkCreate(mostwantedProductiondata);
                    })
                    .then(() => this.MostWanted.update({ Updated: false }, { where: { Updated: true } }))
                    .then(() => this.MostWanted.findAll({ where: { inProduction: true, Deleted: true, } }))
                    .then(mw => {
                        if (mw === null) {
                            return new Promise(function (resolve, reject) {
                                resolve(mw);
                            });
                        }
                        //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
                        var ids = [];
                        for (let i = 0; i < mw.length; i++) {
                            const mwData = mw[i];
                            ids.push(mwData.id);
                        }
                        return this.mostwantedProduction.destroy({ where: { id_production: ids } })


                    })
                    .then(() => {
                        t.commit();
                        return this.responseCorrectStatus(res, 'The site has been published correctly');
                    })
                    .catch((err) => {
                        console.log('Error ==========================>' + err);
                        t.rollback();
                        return this.responseServerErrorStatus(res, err);
                    });

            } catch (e) {
                console.log(e);
                return this.responseServerErrorStatus(res, 'Error');
            }
        });
    }

}

module.exports = MostWantedController;