'use strict';
const DB = require('../utils/DBConnection');
const fs = require('fs');
const path = require('path');
var nodemailer = require('nodemailer');
var ejs = require('ejs');
class FileComplaint {

    constructor() {
        this.config = require('../utils/Config');
        this.DB = new DB();
        this.Configuration = this.DB.Connection.import('../models/PageConfiguration');
        this.modelData = {
            title: '',
            photo: '',
            message: '',
            signature: ''
        };
    }

    getPhoto(imageFile, response) {
        const imageFilename = imageFile;
        const pathfile = `${this.config.server.url}/${imageFilename}`;

        fs.exists(pathfile, (exists) => {
            if (exists)
                response.sendFile(path.resolve(pathfile));
            else
                return this.responseNotFoundStatus(response, { errorType: "No has subido una imagen", error: "" });
        });


    }

    sendEmailFileReport(request, response) {
        const Report = request.body;
        var emailTo;
        try {

            this.Configuration.findById(1)
                .then(report => {
                    emailTo = report.FileAREportEmail;
                    var template = ''
                    try {
                    
                        ejs.renderFile('./public/EmailTemplates/FileReport.html', { Report: Report }, null, function(err, str){
                            if(err)
                            console.log(err)

                             template = str
                        });
                    } catch (error) {
                        console.log(error)
                    }


                    var smtpTrans = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'jonathanmedina1809@gmail.com',
                            pass: 'HaloReach1809.'
                        }
                    });
                    var mailOptions = {

                        to: emailTo,//correo de la base de datos 
                        from: Report.email,
                        subject: 'Report',
                        cc: Report.Q6 ? Report.email : '',
                        html: template

                    };
                    console.log('step 3')



                    smtpTrans.sendMail(mailOptions, function (err) {
                        console.log('sent', emailTo)
                        console.log(Report)
                    });



                })
                .catch(err => {

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                })
        } catch (error) {
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }





    }
    sendEmaiJoinReserve(request, response) {
        const Report = request.body;
        var emailTo;
        try {

            this.Configuration.findById(1)
                .then(report => {
                    emailTo = report.FileAREportEmail;
                    var template = ''
                    try {
                    
                        ejs.renderFile('./public/EmailTemplates/FileReport.html', { Report: Report }, null, function(err, str){
                            if(err)
                            console.log(err)

                             template = str
                        });
                    } catch (error) {
                        console.log(error)
                    }


                    var smtpTrans = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'jonathanmedina1809@gmail.com',
                            pass: 'HaloReach1809.'
                        }
                    });
                    var mailOptions = {

                        to: emailTo,//correo de la base de datos 
                        from: Report.email,
                        subject: 'Join our reserve',

                        html: template

                    };
                    console.log('step 3')



                    smtpTrans.sendMail(mailOptions, function (err) {
                        console.log('sent', emailTo)
                        console.log(Report)
                    });



                })
                .catch(err => {

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                })
        } catch (error) {
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }





    }
    sendEmaiCommend(request, response) {
        const Report = request.body;
        var emailTo;
        try {

            this.Configuration.findById(1)
                .then(report => {
                    emailTo = report.FileAREportEmail;
                    var template = ''
                    try {
                    
                        ejs.renderFile('./public/EmailTemplates/CommendAndComplaint.html', { Report: Report }, null, function(err, str){
                            if(err)
                            console.log(err)

                             template = str
                        });
                    } catch (error) {
                        console.log(error)
                    }


                    var smtpTrans = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'jonathanmedina1809@gmail.com',
                            pass: 'HaloReach1809.'
                        }
                    });
                    var mailOptions = {

                        to: emailTo,//correo de la base de datos 
                        from: Report.email,
                        subject: 'Commend',

                        html: template

                    };
                    console.log('step 3')



                    smtpTrans.sendMail(mailOptions, function (err) {
                        console.log('sent', emailTo)
                        console.log(Report)
                    });



                })
                .catch(err => {

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                })
        } catch (error) {
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }





    }

    sendEmaiComplaint(request, response) {
        const Report = request.body;
        var emailTo;
        try {

            this.Configuration.findById(1)
                .then(report => {
                    emailTo = report.FileAREportEmail;
                    var template = ''
                    try {
                    
                        ejs.renderFile('./public/EmailTemplates/CommendAndComplaint.html', { Report: Report }, null, function(err, str){
                            if(err)
                            console.log(err)

                             template = str
                        });
                    } catch (error) {
                        console.log(error)
                    }


                    var smtpTrans = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'jonathanmedina1809@gmail.com',
                            pass: 'HaloReach1809.'
                        }
                    });
                    var mailOptions = {

                        to: emailTo,//correo de la base de datos 
                        from: Report.email,
                        subject: 'file a complaint',

                        html: template

                    };
                    console.log('step 3')



                    smtpTrans.sendMail(mailOptions, function (err) {
                        console.log('sent', emailTo)
                        console.log(Report)
                    });



                })
                .catch(err => {

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                })
        } catch (error) {
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }





    }


    responseCorrectStatus(response, params) {
        return response.status(200).json(params);
    }

    responseServerErrorStatus(response, error) {
        return response.status(500).json(error);
    }

    responseNotFoundStatus(response, error) {
        return response.status(400).json(error);
    }

}

module.exports = FileComplaint;