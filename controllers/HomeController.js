'use strict';
const DB = require('../utils/DBConnection');
const fs = require('fs');
const path = require('path');
const config = require("../utils/Config");
const sequelize = require("sequelize");
const async = require("async-waterfall");
class HomeController {

    constructor() {
        this.DB = new DB();
        this.HomeBP = this.DB.getConnection().import('../models/HomeModel.js');
        this.HomeP = this.DB.getConnection().import('../models/Production/HomeModel.js');
        this.modelData = {
            id: '',
            name: ''
        };
    }



    update(request, response) {
        try {
            const DataRequest = request.body;
            console.log(DataRequest)
            this.HomeBP.update(DataRequest, {where:{id:1}})
                .then(home => this.responseCorrectStatus(response, home))
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }

    }

    get(response) {
        try {
            this.HomeBP.findById(1)
                .then(home => this.responseCorrectStatus(response, {HomeBP:home}))
                .catch(err => {
                    console.log(err);

                    return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
                });
        } catch (error) {
            console.log(error);
            return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
        }
    }

    login(req, res) {

    }

    resetData() {
        this.userData = {
            id: '',
            name: '',
        }
    }

    responseCorrectStatus(response, params) {
        return response.status(200).json(params);
    }

    responseServerErrorStatus(response, error) {
        return response.status(500).json(error);
    }

    responseNotFoundStatus(response, error) {
        return response.status(400).json(error);
    }

}

module.exports = HomeController;