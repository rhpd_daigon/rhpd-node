"use strict";
const DB = require("../utils/DBConnection");
const fs = require("fs");
const path = require("path");
const config = require("../utils/Config");
const sequelize = require("sequelize");
const async = require("async-waterfall");

class DepartmentController {
  constructor() {
    this.DB = new DB();
    this.police_officers = this.DB.getConnection().import(
      "../models/police_officers"
    );
    this.police_officersProduction = this.DB.getConnection().import(
      "../models/Production/police_officers.js"
    );
    this.ranks = this.DB.getConnection().import("../models/officer_rank.js");


    this.populateQuery = [{ model: this.ranks, as: "rank" }];
  }
  //add officer
  createOfficer(request, response) {
    try {
      const messageDataRequest = request.body;

      this.police_officers
        .create(messageDataRequest)
        .then(officer => {
          return this.responseCorrectStatus(response, officer);
        })
        .catch(err => {
          console.log(err);
          return this.responseServerErrorStatus(response, {
            errorType: "BD",
            error: err
          });
        });
    } catch (error) {
      console.log(error);
      return this.responseServerErrorStatus(response, {
        errorType: "Server",
        error: error
      });
    }
  }

  deleteOfficer(id, response) {
    try {
      this.police_officers.destroy({ where: { id: id } })
        .then(police_officers => {
          return this.responseCorrectStatus(response, 'Officer deleted');
        })
        .catch(err => {
          console.log(err);
          return this.responseServerErrorStatus(response, {
            errorType: "BD",
            error: err
          });
        });
    } catch (error) {
      console.log(error);
      return this.responseServerErrorStatus(response, {
        errorType: "Server",
        error: error
      });
    }
  }

  //update
  updateOfficer(request, id, response) {
    try {
      const DataToUpdate = request.body;
      
      this.police_officers
        .update(DataToUpdate, { where: { id: id } })
        .then(() => this.police_officers.findById(id))
        .then(officer =>
          {return this.responseCorrectStatus(response, officer)}
        )
        .catch(err => {
          console.log(err);
          return this.responseServerErrorStatus(response, {
            errorType: "BD",
            error: err
          });
        });
    } catch (error) {
      console.log(error);
      return this.responseServerErrorStatus(response, {
        errorType: "Server",
        error: error
      });
    }
  }

  getOfficer(id, response) {
    try {
      this.police_officers.findById(id, { include: this.populateQuery })
        .then(model => this.responseCorrectStatus(response, model))
        .catch(err => {
          console.log(err);
          return this.responseServerErrorStatus(response, {
            errorType: "BD",
            error: err
          });
        });
    } catch (error) {
      console.log(error);
      return this.responseServerErrorStatus(response, {
        errorType: "Server",
        error: error
      });
    }
  }

  getModelsPagination(page, fallen, res) {
    try {
      var limit = 10; // number of records per page
      var offset = 0;

      this.police_officers
        .findAndCountAll()
        .then(result => {
          pages = Math.ceil(result.count / limit);
          offset = limit * (page - 1);
          return this.police_officers.findAll({
            limit: limit,
            offset: offset,
            $sort: {
              id: 1
            },
            where: {
              fallen: fallen
            }
          });
        })
        .then(result => {
          return this.reshelpobject(
            200,
            {
              users: result,
              pages
            },
            res
          );
        })
        .catch(err => {
          console.log(err);

          return this.responseServerErrorStatus(response, {
            errorType: "BD",
            error: err
          });
        });
    } catch (error) {
      console.log(error);
      return this.responseServerErrorStatus(response, {
        errorType: "Server",
        error: error
      });
    }
  }

  getOfficersbyrank(req, rank, fallen, response) {
    try {
      var isTrueSet = fallen === "true" ? true : false;
      this.police_officers
        .findAll({
          where: { category: rank, fallen: isTrueSet, Deleted: false },
          include: this.populateQuery
        })
        .then(result => {
          return this.responseCorrectStatus(response, {
            police_officers: result
          });
        })
        .catch(err => {
          return this.responseServerErrorStatus(response, {
            errorType: "BD",
            error: err
          });
        });
    } catch (error) {
      console.log(error);
      return this.responseServerErrorStatus(response, {
        errorType: "Server",
        error: error
      });
    }
  }

  getOfficersbyrankProduction(req, rank, fallen, response) {
    try {
      var isTrueSet = fallen === "true" ? true : false;
      this.police_officersProduction
        .findAll({ where: { category: rank, fallen: isTrueSet }, include: this.populateQuery })
        .then(result => {
          return this.responseCorrectStatus(response, {
            police_officers: result
          });
        })
        .catch(err => {
          return this.responseServerErrorStatus(response, {
            errorType: "BD",
            error: err
          });
        });
    } catch (error) {
      console.log(error);
      return this.responseServerErrorStatus(response, {
        errorType: "Server",
        error: error
      });
    }
  }

  getOfficerProduction(id, response) {
    try {
      this.police_officersProduction.findById(id, { include: this.populateQuery })
        .then(model => this.responseCorrectStatus(response, model))
        .catch(err => {
          console.log(err);
          return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
        });
    } catch (error) {
      console.log(error);
      return this.responseServerErrorStatus(response, { errorType: "Server", error: error });
    }
  }

  getOfficersbyrankFallen(req, fallen, response) {
    try {
      var isTrueSet = fallen === "true" ? true : false;

      this.police_officers
        .findAll({
          where: { fallen: isTrueSet, Deleted: false },
          include: this.populateQuery
        })
        .then(result => {
          return this.responseCorrectStatus(response, {
            police_officers: result
          });
        })
        .catch(err => {
          return this.responseServerErrorStatus(response, {
            errorType: "BD",
            error: err
          });
        });
    } catch (error) {
      console.log(error);
      return this.responseServerErrorStatus(response, {
        errorType: "Server",
        error: error
      });
    }
  }

  getOfficersbyrankProductionFallen(req, fallen, response) {
    try {
      var isTrueSet = fallen === "true" ? true : false;
      this.police_officersProduction
        .findAll({ where: { fallen: isTrueSet }, include: this.populateQuery })
        .then(result => {
          return this.responseCorrectStatus(response, {
            police_officers: result
          });
        })
        .catch(err => {
          return this.responseServerErrorStatus(response, {
            errorType: "BD",
            error: err
          });
        });
    } catch (error) {
      console.log(error);
      return this.responseServerErrorStatus(response, {
        errorType: "Server",
        error: error
      });
    }
  }

  addImage(request, id, oldImage, response) {
    const params = request.body;
    var file_name = "officer.png";
    if (request.files) {
      const file_path = request.files.image.path; //ruta de la imagen
      file_name = path.basename(file_path); //obtener solo el nombre de la imagen

      const ext_split = file_name.split(".");
      const file_ext = ext_split[1]; //obtener extencion de la imagen

      if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
        this.police_officers
          .update({ photo: file_name }, { where: { id: id } })
          .then(() => {

            if (oldImage === null || oldImage === file_name) {
              return this.responseCorrectStatus(response, 'Done');
            }
            else {
              if (oldImage !== "officer.png") {
                const rootPath = path.normalize(`${__dirname}/public/uploads/officers/${oldImage}`);
                fs.exists(rootPath, exists => {
                  if (exists) {
                    fs.unlink(rootPath, err => {
                      if (err) {
                        console.log(err);
                      }
                      else {
                        console.log('file removed done');
                        return this.responseCorrectStatus(response, 'Done');
                      }
                    });
                  }
                  else {
                    return this.responseCorrectStatus(response, 'Done with fails');
                  }

                });
              }
              else {
                return this.responseCorrectStatus(response, 'Done');
              }
            }
          })
          .catch(err => {
            console.log(err)
            return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
          });
      } else {
        return this.responseServerErrorStatus(response, { errorType: "Server", error: "file extension is not valid" });
      }
    } else {
      return this.responseNotFoundStatus(response, { errorType: " not found", error: "image not found" });
    }
  }

  addImageFallen(request, id, oldImage, response) {
    const params = request.body;
    var file_name = "fallen-officer.png";
    if (request.files) {
      const file_path = request.files.image.path; //ruta de la imagen
      file_name = path.basename(file_path); //obtener solo el nombre de la imagen

      const ext_split = file_name.split(".");
      const file_ext = ext_split[1]; //obtener extencion de la imagen

      if (file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
        this.police_officers
          .update({ photo: file_name }, { where: { id: id } })
          .then(() => {

            if (oldImage === null || oldImage === file_name) {
              return this.responseCorrectStatus(response, 'Done');
            }
            else {
              if (oldImage !== "fallen-officer.png") {
                const rootPath = path.normalize(`./public/uploads/fallen-officers/${oldImage}`);
                fs.exists(rootPath, exists => {
                  if (exists) {
                    fs.unlink(rootPath, err => {
                      if (err) {
                        console.log(err);
                      }
                      else {
                        console.log('file removed done');
                        return this.responseCorrectStatus(response, 'Done with fails');
                      }
                    });
                  }
                  else {
                    return this.responseCorrectStatus(response, 'Done with fails');
                  }

                });
              }
              else {
                return this.responseCorrectStatus(response, 'Done');
              }
            }
          })
          .catch(err => {
            console.log(err)
            return this.responseServerErrorStatus(response, { errorType: "BD", error: err });
          });
      } else {
        return this.responseServerErrorStatus(response, { errorType: "Server", error: "file extension is not valid" });
      }
    } else {
      return this.responseNotFoundStatus(response, { errorType: " not found", error: "image not found" });
    }
  }


  getImageFile(request, file, response) {
    let pathfile;
    if (file !== undefined) pathfile = "./public/uploads/officers/" + file;
    else pathfile = "./public/uploads/officers/officer.png";
    fs.exists(pathfile, exists => {
      if (exists) response.sendFile(path.resolve(pathfile));
      else
        response.sendFile(path.resolve("./public/uploads/officers/officer.png"));
    });
  }

  getImageFallen(request, file, response) {
    let pathfile;
    if (file !== undefined)
      pathfile = "./public/uploads/fallen-officers/" + file;
    else pathfile = "./public/uploads/fallen-officers/fallen-officer.png";
    fs.exists(pathfile, exists => {
      if (exists) response.sendFile(path.resolve(pathfile));
      else
        response.sendFile(path.resolve("./public/uploads/fallen-officers/fallen-officer.png"));
    });
  }

  responseCorrectStatus(response, params) {
    return response.status(200).json(params);
  }

  responseServerErrorStatus(response, error) {
    return response.status(500).json(error);
  }

  responseNotFoundStatus(response, error) {
    return response.status(400).json(error);
  }

  ProductionOurDepartment(req, res) {
    this.DB.Connection.transaction().then(t => {
      try {

        Promise.all([
          this.police_officers
            .findAll({ where: { inProduction: true, Updated: false, Deleted: false, fallen: false } }, { transaction: t }),
          this.police_officersProduction.findAll({ where: { fallen: false } }, { transaction: t }),
        ])
          .then(result => {

            let officerProduction = [];
            const officers = result[0];
            const officersProductionvalues = result[1];
            //Buscar los datos repetidos para dejar solo los que se van a insertar
            for (let i = 0; i < officersProductionvalues.length; i++) {
              const index = officers.findIndex(o => o.id === officersProductionvalues[i].id);
              officers.splice(index, 1);
            }
            if (officers.length > 0) {
              officers.forEach(officer => {
                //buscar los datos en produccion para no insertar datos repetidos
                let officerP = {};
                officerP.id = officer.id;
                officerP.name = officer.name;
                officerP.photo = officer.photo;
                officerP.division = officer.division;
                officerP.bio = officer.bio;
                officerP.fallen = officer.fallen;
                officerP.email = officer.email;
                officerP.id_rank = officer.id_rank;
                officerP.category = officer.category;
                officerP.id_production = officer.id;
                officerProduction.push(officerP);
              });
            }

            return this.police_officersProduction.bulkCreate(officerProduction);


          })
          .then(() => this.police_officers.findAll({ where: { inProduction: true, Updated: true, fallen: false } }, { transaction: t }))
          .then(officers => {
            if (officers.length === 0) {
              return new Promise(function (resolve, reject) {
                resolve(officers);
              });
            }
            //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
            var ids = [];
            for (let i = 0; i < officers.length; i++) {
              const officer = officers[i];
              ids.push(officer.id);
            }
            return this.police_officersProduction.destroy({ where: { id_production: ids } })
          })
          .then(() => this.police_officers.findAll({ where: { inProduction: true, Updated: true, fallen: false } }, { transaction: t }))
          .then((officers) => {
            if (officers.length === 0) {
              return new Promise(function (resolve, reject) {
                resolve(officers);
              });
            }
            let officerProduction = [];
            if (officers.length > 0) {
              officers.forEach(officer => {
                //buscar los datos en produccion para no insertar datos repetidos
                let officerP = {};
                officerP.id = officer.id;
                officerP.name = officer.name;
                officerP.photo = officer.photo;
                officerP.division = officer.division;
                officerP.bio = officer.bio;
                officerP.fallen = officer.fallen;
                officerP.id_rank = officer.id_rank;
                officerP.category = officer.category;
                officerP.id_production = officer.id;
                officerProduction.push(officerP);
              });
            }

            return this.police_officersProduction.bulkCreate(officerProduction);
          })
          .then(() => this.police_officers.update({ Updated: false }, { where: { Updated: true, fallen: false } }))
          .then(() => this.police_officers.findAll({ where: { inProduction: true, Deleted: true, fallen: false } }))
          .then(officers => {
            if (officers === null) {
              return new Promise(function (resolve, reject) {
                resolve(officers);
              });
            }
            //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
            var ids = [];
            for (let i = 0; i < officers.length; i++) {
              const officer = officers[i];
              ids.push(officer.id);
            }
            return this.police_officersProduction.destroy({ where: { id_production: ids } })


          })
          .then(() => {
            t.commit();
            return this.responseCorrectStatus(res, 'The site has been published correctly');
          })
          .catch((err) => {
            console.log(err);
            t.rollback();
            return this.responseServerErrorStatus(res, err);
          });

      } catch (e) {
        console.log(e);
        return this.responseServerErrorStatus(res, 'Error');
      }
    });
  }

  ProductionOurDepartmentFallen(req, res) {
    this.DB.Connection.transaction().then(t => {
      try {
        Promise.all([
          this.police_officers
            .findAll({ where: { inProduction: true, Updated: false, Deleted: false, fallen: true } }, { transaction: t }),
          this.police_officersProduction.findAll({ where: { fallen: true } }, { transaction: t }),
        ])
          .then(result => {

            let officerProduction = [];
            const officers = result[0];
            const officersProductionvalues = result[1];
            //Buscar los datos repetidos para dejar solo los que se van a insertar
            for (let i = 0; i < officersProductionvalues.length; i++) {
              const index = officers.findIndex(o => o.id === officersProductionvalues[i].id);
              officers.splice(index, 1);
            }
            if (officers.length > 0) {
              officers.forEach(officer => {
                //buscar los datos en produccion para no insertar datos repetidos
                let officerP = {};
                officerP.id = officer.id;
                officerP.name = officer.name;
                officerP.photo = officer.photo;
                officerP.division = officer.division;
                officerP.bio = officer.bio;
                officerP.fallen = officer.fallen;
                officerP.email = officer.email;
                officerP.id_rank = officer.id_rank;
                officerP.category = officer.category;
                officerP.id_production = officer.id;
                officerProduction.push(officerP);
              });
            }

            return this.police_officersProduction.bulkCreate(officerProduction);


          })
          .then(() => this.police_officers.findAll({ where: { inProduction: true, Updated: true, fallen: true } }, { transaction: t }))
          .then(officers => {
            if (officers.length === 0) {
              return new Promise(function (resolve, reject) {
                resolve(officers);
              });
            }
            //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
            var ids = [];
            for (let i = 0; i < officers.length; i++) {
              const officer = officers[i];
              ids.push(officer.id);
            }
            return this.police_officersProduction.destroy({ where: { id_production: ids } })
          })
          .then(() => this.police_officers.findAll({ where: { inProduction: true, Updated: true, fallen: true } }, { transaction: t }))
          .then((officers) => {
            if (officers.length === 0) {
              return new Promise(function (resolve, reject) {
                resolve(officers);
              });
            }
            let officerProduction = [];
            if (officers.length > 0) {
              officers.forEach(officer => {
                //buscar los datos en produccion para no insertar datos repetidos
                let officerP = {};
                officerP.id = officer.id;
                officerP.name = officer.name;
                officerP.photo = officer.photo;
                officerP.division = officer.division;
                officerP.bio = officer.bio;
                officerP.fallen = officer.fallen;
                officerP.id_rank = officer.id_rank;
                officerP.category = officer.category;
                officerP.id_production = officer.id;
                officerProduction.push(officerP);
              });
            }

            return this.police_officersProduction.bulkCreate(officerProduction);
          })
          .then(() => this.police_officers.update({ Updated: false }, { where: { Updated: true, fallen: true } }))
          .then(() => this.police_officers.findAll({ where: { inProduction: true, Deleted: true, fallen: true } }))
          .then(officers => {
            if (officers === null) {
              return new Promise(function (resolve, reject) {
                resolve(officers);
              });
            }
            //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
            var ids = [];
            for (let i = 0; i < officers.length; i++) {
              const officer = officers[i];
              ids.push(officer.id);
            }
            return this.police_officersProduction.destroy({ where: { id_production: ids } })


          })
          .then(() => {
            t.commit();
            return this.responseCorrectStatus(res, 'The site has been published correctly');
          })
          .catch((err) => {
            console.log('Error ==========================>' + err);
            t.rollback();
            return this.responseServerErrorStatus(res, err);
          });

      } catch (e) {
        console.log(e);
        return this.responseServerErrorStatus(res, 'Error');
      }
    });
  }

  getRanksByCategory(category, res) {

  }

  CreateRank(req, res) {

  }

  deleteRank(id, res) {

  }
}

module.exports = DepartmentController;
