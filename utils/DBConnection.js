"use strict";
/*requiring mongodb node modules */
const Sequelize = require('sequelize');
class Db {

    constructor() {
        this.config = require('./Config');
        this.db = this.config.db;
        this.Connection;
        this.onConnect();
        
    }

    onConnect() {
        this.Connection = new Sequelize(this.db.db, this.db.user, this.db.pass, {
            host: this.db.host,
            dialect: 'mysql',
            port: 3306,
            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000
            },
            logging: false,
            operatorsAliases: false
        });
    }

    getConnection(){
        return this.Connection;
    }

    onConnectMongo(){

    }
}
module.exports = Db;