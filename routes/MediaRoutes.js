"use strict";
const MediaController = require('../controllers/MediaController');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './public/uploads/media'
});


class MediaRoutes {

    constructor(app) {
        this.app = app;
        this._Media = new MediaController();

    }

    appRoutes() {

        //GetImage
        this.app.get('/Media/:file', (req, res) => {
            const file = req.params.file;

            this._Media.getPhoto(file, res);
        });
        //UploadImage
        this.app.post('/Media/image/:id', md_upload, (req, res) => {
            const id = req.params.id;
            const oldImage = req.headers.oldimage;
            this._Media.photo(req, id, oldImage, res);
            
        });
        //Get all Media

        this.app.get('/Media', (req, res) => {
            this._Media.getAll(res);
        });

        //Get One Media
        this.app.get('/Media/one/:id', (req, res) => {
            const id = req.params.id;
            this._Media.getOne(id, res);
        });
        //Create Media
        this.app.post('/Media', (req, res) => {
            this._Media.create(req, res);
        });
        //Update Media
        this.app.put('/Media/:id', md_upload, (req, res) => {
            const id = req.params.id;
            this._Media.update(req, id, res);
        });
       
        //Publish Media
        this.app.post('/production/Media', (req, res) => {
            this._Media.Production(req, res);
        });
        //Get media Published

        this.app.get('/production/Media', (req, res) => {
            this._Media.getAllProduction(res);
        });



    }
    routesConfig() {
        this.appRoutes();
    }

}

module.exports = MediaRoutes;