"use strict";
const HomeController = require('../controllers/HomeController');
const md_auth = require('../middlewares/auth');
const multipart = require('connect-multiparty');

//md_auth.ensureAuth
class routesChiefMessage {
    constructor(app) {
        this.app = app;
        this.home = new HomeController();
    }

    appRoutes() {
       
        this.app.get('/home/get', (req,res) => {
            this.home.get(res)
        });

        this.app.get('production/home', (req,res) => {
            this.home.getDataProduction(res)
        });

        this.app.put('/home/update', (req,res) => {
          this.home.update(req,res);  
        });

    }
    routesConfig() {
        this.appRoutes();
    }

    responseNotFoundStatus(response, error) {
        return response.status(404).json(error);
    }

    // Production(req, res) {
    //     this.DB.Connection.transaction().then(t => {
    //         try {
    //             const _that = this;
    //             async(
    //                 [
    //                     (done) => {
    //                         this.media
    //                             .findAll({ where: { inProduction: true, Updated: false, Deleted: false } })
    //                             .then(media => {
    //                                 if (media.length === 0)
    //                                     done(null, media);
    //                                 //buscar los datos en produccion para no insertar datos repetidos
    //                                 this.mediaProduction
    //                                     .findAll({})
    //                                     .then(mediaPro => {
    //                                         let mediaProductiondata = [];
    //                                         //Buscar los datos repetidos para dejar solo los que se van a insertar
    //                                         for (let i = 0; i < mediaPro.length; i++) {
    //                                             const index = media.findIndex(o => o.id === mediaPro[i].id);
    //                                             media.splice(index, 1);
    //                                         }
    //                                         if (media.length > 0) {
    //                                             media.forEach(mediadata => {
    //                                                 //buscar los datos en produccion para no insertar datos repetidos
    //                                                 let mediaP = {};
    //                                                 mediaP.id = mediadata.id;
    //                                                 mediaP.title = mediadata.title;
    //                                                 mediaP.photo = mediadata.photo;
    //                                                 mediaP.descriptionLong = mediadata.descriptionLong;
    //                                                 mediaP.descriptionShort = mediadata.descriptionShort;
    //                                                 mediaP.videoUrl = mediadata.videoUrl;
    //                                                 mediaP.publishDate = mediadata.publishDate;

    //                                                 mediaProductiondata.push(mediaP);
    //                                             });
    //                                         }

    //                                         _that.mediaProduction.bulkCreate(mediaProductiondata)
    //                                             .then(() => { done(null); })
    //                                             .catch(err => {
    //                                                 console.log(err);
    //                                                 t.rollback();
    //                                                 done(err, null);
    //                                             });
    //                                     });
    //                             });
    //                     },
    //                     (done) => {
    //                         _that.media
    //                             .findAll({ where: { inProduction: true, Updated: true } })
    //                             .then(media => {
    //                                 if (media.length === 0)
    //                                     done(null, media);
    //                                 //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
    //                                 for (let i = 0; i < media.length; i++) {
    //                                     const mediaData = media[i];
    //                                     _that.mediaProduction.destroy({ where: { id_production: mediaData.id } })
    //                                         .then(() => {
    //                                             if (i === media.length - 1) {
    //                                                 done(null, media);
    //                                             }
    //                                         })
    //                                         .catch((err) => {
    //                                             console.log(err);
    //                                             t.rollback();
    //                                             done(err, null);
    //                                         });
    //                                 }
    //                             });
    //                     },
    //                     (media, done) => {
    //                         if (media.length === 0)
    //                             done(null);
    //                         let mediaProductiondata = [];
    //                         if (media.length > 0) {
    //                             media.forEach(mediadata => {
    //                                 //buscar los datos en produccion para no insertar datos repetidos
    //                                 let mediaP = {};
    //                                 mediaP.id = mediadata.id;
    //                                 mediaP.title = mediadata.title;
    //                                 mediaP.photo = mediadata.photo;
    //                                 mediaP.descriptionLong = mediadata.descriptionLong;
    //                                 mediaP.descriptionShort = mediadata.descriptionShort;
    //                                 mediaP.videoUrl = mediadata.videoUrl;
    //                                 mediaP.publishDate = mediadata.publishDate;

    //                                 mediaProductiondata.push(mediaP);
    //                             });
    //                         }

    //                         _that.mediaProduction.bulkCreate(mediaProductiondata)
    //                             .then(() => {

    //                                 _that.media.update({ Updated: false }, { where: { Updated: true } })
    //                                     .then(() => {
    //                                         done(null);
    //                                     })
    //                                     .catch(err => {
    //                                         console.log(err);
    //                                         t.rollback();
    //                                         done(err, null);
    //                                     });
    //                             })
    //                             .catch(err => {
    //                                 console.log(err);
    //                                 t.rollback();
    //                                 done(err, null);
    //                             });


    //                     },
    //                     (done) => {
    //                         _that.media
    //                             .findAll({ where: { inProduction: true, Deleted: true } })
    //                             .then(media => {
    //                                 if (media === null)
    //                                     return _that.responseCorrectStatus(res, "Done");
    //                                 //buscar los datos que se actualizaran para borrarlos en produccion y volverlos a insertar
    //                                 for (let i = 0; i < media.length; i++) {
    //                                     const mediadata = media[i];
    //                                     _that.mediaProduction.destroy({ where: { id_production: mediadata.id } })
    //                                         .then(() => {
    //                                             if (i === media.length - 1) {
    //                                                 t.commit();
    //                                                 return _that.responseCorrectStatus(res, 'The site has been published correctly');
    //                                             }
    //                                         })
    //                                         .catch((err) => {
    //                                             console.log(err);
    //                                             t.rollback();
    //                                             done(err, null);
    //                                         });
    //                                 }
    //                             });
    //                     }
    //                 ],
    //                 (err) => {
    //                     console.log("_that err" + " " + err);
    //                     return _that.responseServerErrorStatus(res, err);
    //                 }
    //             );
    //         } catch (e) {
    //             console.log(e);
    //         }
    //     });
    // }

}

module.exports = routesChiefMessage;