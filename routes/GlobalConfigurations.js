"use strict";
const GlobalController = require('../controllers/GlobalConfigurationsController');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './public/uploads/headers'
});
class GlobalRoutes {
    constructor(app) {
        this.app = app;
        this.GlobalController = new GlobalController();

    }

    appRoutes() {


        this.app.get('/Global/header/:page', (req, res) => {
            const page = req.params.page;
            this.GlobalController.getTitle(page, res);
        });

        this.app.put('/Global/header/:page', (req, res) => {
            const page = req.params.page;
            this.GlobalController.UpdateTitle(req, page, res);
        });

        this.app.get('/Dispatches', (req, res) => {
            this.GlobalController.getDispatches(res);
        });

        this.app.get('/Global/:search?', (req, res) => {
            const text = req.params.search;
            if (text !== '' && text !== undefined) {
                this.GlobalController.sercher(text, res);
            }
            else {
                res.status(200).json([]);
            }
        });

        this.app.get('/production/Global/:search?', (req, res) => {
            const text = req.params.search;
            if (text !== '' && text !== undefined) {
                this.GlobalController.sercherProduction(text, res);
            }
            else {
                res.status(200).json([]);
            }
        });

        this.app.post('/production/Global', (req, res) => {

            this.GlobalController.productionHeaders( res);

        });

        this.app.get('/production/Global/header/:page', (req, res) => {
            const page = req.params.page;
            this.GlobalController.getTitleProduction(page, res);
        });

          //GetImage
          this.app.get('/Global/image/:file', (req, res) => {
            const file = req.params.file;

            this.GlobalController.getPhoto(file, res);
        });

        //UploadImage
        this.app.post('/Global/image/:page', md_upload, (req, res) => {
            const id = req.params.page;
            const oldImage = req.headers.oldimage;
            this.GlobalController.photo(req, id, oldImage, res);
            
        });
    }
    routesConfig() {
        this.appRoutes();
    }

}

module.exports = GlobalRoutes;