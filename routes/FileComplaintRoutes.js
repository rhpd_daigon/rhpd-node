"use strict";
const FileComplaintController = require('../controllers/FileComplaint');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './uploads/users'
});
class FileComplaintRoutes {
    constructor(app) {
        this.app = app;
        this.FileComplaint = new FileComplaintController();
    }

    appRoutes() {
       
       
        this.app.post('/fileComplaint', (req,res) => {
            this.FileComplaint.sendEmailFileReport(req,res);
        });
        this.app.post('/fileComplaint/joinReserve', (req,res) => {
            this.FileComplaint.sendEmaiJoinReserve(req,res);
        });
        this.app.post('/fileComplaint/commend', (req,res) => {
            this.FileComplaint.sendEmaiCommend(req,res);
        });
        this.app.post('/fileComplaint/complaint', (req,res) => {
            this.FileComplaint.sendEmaiComplaint(req,res);
        });
        // this.app.post('/fileReport', (req,res) => {
        //     this.FileComplaint.senEmailFileReport(req,res);
        // });
        // this.app.post('/fileCommsend', (req,res) => {
        //     this.FileComplaint.senEmailFileReport(req,res);
        // });
     
     
    }
    routesConfig() {
        this.appRoutes();
    }

}

module.exports = FileComplaintRoutes;