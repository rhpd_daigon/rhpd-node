"use strict";
const EventController = require('../controllers/EventController');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './public/uploads/event'
});


class EventRoutes {

    constructor(app) {
        this.app = app;
        this._Event = new EventController();

    }

    appRoutes() {

        //GetImage
        this.app.get('/Event/:file', (req, res) => {
            const file = req.params.file;

            this._Event.getPhoto(file, res);
        });
        //UploadImage
        this.app.post('/Event/image/:id', md_upload, (req, res) => {
            const id = req.params.id;
            const oldImage = res.getHeader('oldImage')
            // console.log(req)
            this._Event.photo(req, id, oldImage, res);
            
        });

        //UploadImages
        this.app.post('/Event/Images/:id', md_upload, (req, res) => {
            const id = req.params.id;
            const oldImage = req.headers.oldimage;
            this._Event.Addphotos(req,id,res);
            
        });

        this.app.put('/Event/Images/:id', md_upload, (req, res) => {
            const id = req.params.id;
            const oldImage = req.headers.oldimage;
            this._Event.UpdateImages(req,id,oldImage,res);
            
        });

        this.app.delete('/Event/Images/:id/:name', (req, res) => {
            const id = req.params.id;
            const name = req.params.name;
            this._Event.deleteImages( id,name,res);
        });

          //UploadImages
          this.app.put('/Event/Images/:id', md_upload, (req, res) => {
            const id = req.params.id;
           
            this._Event.UpdateImages(req,id,res);
            
        });

        //Get all Event
        this.app.get('/Event', (req, res) => {
            this._Event.getAll(res);
        });

        //Get One Event
        this.app.get('/Event/one/:title', (req, res) => {
            const id = req.params.title;
            this._Event.getOneByTitleUrl(id, res);
        });
        //Create Event
        this.app.post('/Event', (req, res) => {
            this._Event.create(req, res);
        });
        //Update Event
        this.app.put('/Event/:id', md_upload, (req, res) => {
            const id = req.params.id;
            this._Event.update(req, id, res);
        });
       
        //Publish Event
        this.app.post('/production/Event', (req, res) => {
            this._Event.Production(req, res);
        });
        //Get Event Published

        this.app.get('/production/Event', (req, res) => {
            this._Event.getAllProduction(res);
        });

        this.app.get('/production/Event/one/:title', (req, res) => {
            const id = req.params.title;
            this._Event.getOneProduction(id,res);
        });



    }
    routesConfig() {
        this.appRoutes();
    }

}

module.exports = EventRoutes;