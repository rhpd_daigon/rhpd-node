"use strict";
const ChiefMessageController = require('../controllers/ChiefMessageController');
const md_auth = require('../middlewares/auth');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './public/uploads/chief_message'
});
//md_auth.ensureAuth
class routesChiefMessage {
    constructor(app) {
        this.app = app;
        this.Chief = new ChiefMessageController();
    }

    appRoutes() {

        this.app.get('/chief-message/get', (req, res) => {
            this.Chief.getData(res)
        });

        this.app.get('/production/chief-message', (req, res) => {
            this.Chief.getDataProduction(res)
        });

        this.app.get('/chief-message/:image', (req, res) => {
            const image = req.params.image;
            this.Chief.getPhoto(req, image, res)
        });

        this.app.post('/chief-message/upload/:id/:action', md_upload, (req, res) => {
            const id = req.params.id;
            const oldImage = req.headers.oldimage;
            const action = req.params.action;
            this.Chief.UploadPhoto(req, id, action, oldImage, res);

        });

        this.app.put('/chief-message/update', (req, res) => {
            this.Chief.updateData(req, res);
        });

        this.app.post('/production/chief-message', (req, res) => {
            this.Chief.production(res);
        });

    }
    routesConfig() {
        this.appRoutes();
    }

    responseNotFoundStatus(response, error) {
        return response.status(404).json(error);
    }

}

module.exports = routesChiefMessage;