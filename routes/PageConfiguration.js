"use strict";
const PageController = require('../controllers/PageConfiguration');
const multipart = require('connect-multiparty');

class PageConfigurationRoutes {
    constructor(app) {
        this.app = app;
        this.PageConfiguration = new PageController();
    }

    appRoutes() {
       
        this.app.get('/configuration', (req,res) => {
            this.PageConfiguration.get(res)
        });
     
        this.app.put('/configuration', (req,res) => {
            this.PageConfiguration.updateModel(req,res)
        });

        this.app.get('/production/configuration/get', (req, res) => {
            this.PageConfiguration.getProduction(res);
        });

        this.app.post('/production/configuration', (req, res) => {
            this.PageConfiguration.production(res);
        });

     
    }
    routesConfig() {
        this.appRoutes();
    }

}

module.exports = PageConfigurationRoutes;