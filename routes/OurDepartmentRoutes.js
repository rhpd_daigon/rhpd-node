"use strict";
const DepartmentController = require('../controllers/DepartmentControler');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './public/uploads/officers'
});

const md_uploadFallenOfficer = multipart({
    uploadDir: './public/uploads/fallen-officers'
});
class OurDepartmentRoutes {
    constructor(app) {
        this.app = app;
        this.OurDepartment = new DepartmentController();
    
    }

    appRoutes() {
       
        this.app.get('/OurDepartment/:file', (req,res) => {
            const file = req.params.file;
            this.OurDepartment.getImageFile(req,file,res);
        });

        this.app.get('/OurDepartment/fallen/:file', (req,res) => {
            const file = req.params.file;
            this.OurDepartment.getImageFallen(req,file,res);
        });

        this.app.post('/OurDepartment/image/fallen/:id',md_uploadFallenOfficer, (req,res) => {
            const id = req.params.id;
            const oldImage = req.headers.oldimage;
            this.OurDepartment.addImageFallen(req,id,oldImage,res);
        });

        this.app.get('/production/OurDepartment/Officers/:rank/:fallen', (req,res) => {
            const rank = req.params.rank;
            const fallen = req.params.fallen;
            this.OurDepartment.getOfficersbyrankProduction(req,rank,fallen,res);
        });

        this.app.get('/OurDepartment/Officers/:rank/:fallen', (req,res) => {
            const rank = req.params.rank;
            const fallen = req.params.fallen;
            this.OurDepartment.getOfficersbyrank(req,rank,fallen,res);
        });

        this.app.get('/production/OurDepartment/fallen/all/:fallen', (req,res) => {
           
            const fallen = req.params.fallen;
            this.OurDepartment.getOfficersbyrankProductionFallen(req,fallen,res);
        });

        this.app.get('/OurDepartment/fallen/all/:fallen', (req,res) => {
            const fallen = req.params.fallen;
            
            this.OurDepartment.getOfficersbyrankFallen(req,fallen,res);
        });
        this.app.get('/production/OurDepartment/one/:id', (req,res) => {
            const id = req.params.id;
            this.OurDepartment.getOfficerProduction(id,res);
        });

        

        this.app.post('/OurDepartment', (req,res) => {
            this.OurDepartment.createOfficer(req,res);
        });
        
        this.app.post('/OurDepartment/image/:id', md_upload , (req,res) => {
            const id = req.params.id;
            const oldImage = req.headers.oldimage;
            this.OurDepartment.addImage(req,id,oldImage, res);
        });

        this.app.put('/OurDepartment/update/:id' ,(req,res) => {
            const id = req.params.id;
            this.OurDepartment.updateOfficer(req,id,res);
        });

        this.app.delete('/OurDepartment/:id', (req,res) => {
            const id = req.params.id;
            this.OurDepartment.deleteOfficer(id,res)
        });

        this.app.get('/OurDepartment/one/:id', (req,res) => {
            const id = req.params.id;
            this.OurDepartment.getOfficer(id,res);
        });

        this.app.get('/OurDepartment/:page', (req,res) => {
            const page = req.params.page;
            if (!page) {
                page = 1;
            }
            this.OurDepartment.getModelsPagination(page,res);
        });

        this.app.post('/production/OurDepartment', (req,res) => {
           
            // console.log(req)
            this.OurDepartment.ProductionOurDepartment(req,res);
        });

        this.app.post('/production/OurDepartment/fallen', (req,res) => {
           
            // console.log(req)
            this.OurDepartment.ProductionOurDepartmentFallen(req,res);
        });

        // rank officers
        this.app.get('/ranks/:category', (req,res) => {
            const category = req.params.category;
            if (!category) {
                category = 1;
            }
            this.OurDepartment.getRanksByCategory(category,res)
        });

        this.app.post('/ranks/OurDepartment', (req,res) => {
           
            // console.log(req)
            this.OurDepartment.CreateRank(req,res);
        });

        this.app.delete('/ranks/:id', (req,res) => {
            const id = req.params.id;
            this.OurDepartment.deleteRank(id,res)
        });
       
    }
    routesConfig() {
        this.appRoutes();
    }

}

module.exports = OurDepartmentRoutes;