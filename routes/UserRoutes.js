"use strict";
const UserController = require('../controllers/UserController');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './public/uploads/Officers'
});

class UserRoutes {
    constructor(app) {
        this.app = app;
        this.User = new UserController();
    
    }

    appRoutes() {
        this.app.post('/Access-Control', (req,res) => {
            this.User.loginWeb(req,res);
        });
    }
    routesConfig() {
        this.appRoutes();
    }

}

module.exports = UserRoutes;