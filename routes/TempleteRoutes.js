"use strict";
const templeteController = require('../controllers/TempleateContoller');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './uploads/users'
});
class routesUser {
    constructor(app) {
        this.app = app;
        this.templete = new templeteController();
    }

    appRoutes() {
       
        this.app.get('/models', (req,res) => {
            this.templete.getAllUsers()
        });
        this.app.post('/model', (req,res) => {
            this.templete.createModel(req,res);
        });
        this.app.post('/model/image',md_upload, (req,res) => {
            this.templete.createModel(req,res);
        });
        this.app.put('/model/:id', (req,res) => {
            const id = req.params.id;
            this.templete.updateModel(req,id,res)
        });

        this.app.delete('/model/:id', (req,res) => {
            const id = req.params.id;
            this.templete.deleteModel(id,res);
        });

        this.app.get('/model/:id', (req,res) => {
            const id = req.params.id;
            this.templete.getModel(id,res);
        });

        this.app.get('/models/:page', (req,res) => {
            const page = req.params.page;
            if (!page) {
                page = 1;
            }
            this.templete.getModelsPagination(page,res);
        });
    }
    routesConfig() {
        this.appRoutes();
    }

}

module.exports = routesUser;