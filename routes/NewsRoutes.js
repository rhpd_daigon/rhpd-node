"use strict";
const EventController = require('../controllers/NewsController');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './public/uploads/news'
});


class NewsRoute {

    constructor(app) {
        this.app = app;
        this._News = new EventController();

    }

    appRoutes() {

        //GetImage
        this.app.get('/api/News/:file', (req, res) => {
            const file = req.params.file;

            this._News.getPhoto(file, res);
        });
        //UploadImage
        this.app.post('/api/News/image/:id', md_upload, (req, res) => {
            const id = req.params.id;
            const oldImage = req.headers.oldimage;
            // console.log(req)
            this._News.photo(req, id, oldImage, res);
            
        });

        //UploadImages
        this.app.post('/api/News/Images/:id', md_upload, (req, res) => {
            const id = req.params.id;
           
            this._News.Addphotos(req,id,res);
            
        });

          //UploadImages
          this.app.put('/api/News/Images/:id', md_upload, (req, res) => {
            const id = req.params.id;
            const oldImage = req.headers.oldimage;
            this._News.UpdateImages(req,id,oldImage,res);
            
        });

        this.app.delete('/api/News/Images/:id/:name', (req, res) => {
            const id = req.params.id;
            const name = req.params.name;
            this._News.deleteImages( id,name,res);
        });

        //Get all News
        this.app.get('/api/News', (req, res) => {
            this._News.getAll(res);
        });

        //Get One News
        this.app.get('/api/News/one/:title', (req, res) => {
            const id = req.params.title;
            this._News.getOneByTitleUrl(id, res);
        });
        //Create News
        this.app.post('/api/News', (req, res) => {
            this._News.create(req, res);
        });
        //Update News
        this.app.put('/api/News/:id', md_upload, (req, res) => {
            const id = req.params.id;
            this._News.update(req, id, res);
        });


       
        //Publish News
        this.app.post('/api/production/News', (req, res) => {
            this._News.Production(req, res);
        });
        //Get News Published

        this.app.get('/api/production/News', (req, res) => {
            this._News.getAllProduction(res);
        });

        this.app.get('/api/production/News/one/:title', (req, res) => {
            const id = req.params.title;
            this._News.getOneProduction(id,res);
        });



    }
    routesConfig() {
        this.appRoutes();
    }

}

module.exports = NewsRoute;