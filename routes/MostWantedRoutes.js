"use strict";
const MostWantedController = require('../controllers/MostWantedController');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './public/uploads/most_wanted'
});
class routesMostWanted {
    constructor(app) {
        this.app = app;
        this.most_wanted = new MostWantedController();
    }

    appRoutes() {
        this.app.get('/most-wanted/:file', (req,res) => {
            const file = req.params.file;
            this.most_wanted.getImageFile(req,file,res);
        });
        
        this.app.get('/most-wanted', (req,res) => {
            this.most_wanted.getAllWanted(res)
        });
        this.app.post('/most-wanted', (req,res) => {
            this.most_wanted.create(req,res);
        });
        this.app.post('/most-wanted/image/:id',md_upload, (req,res) => {
            const id = req.params.id;
            const oldImage = req.headers.oldimage;
            this.most_wanted.addImage(req, id,oldImage,res);
        });
        this.app.put('/most-wanted/:id', (req,res) => {
            const id = req.params.id;
            this.most_wanted.updateModel(req,id,res)
        });

        this.app.delete('/most-wanted/:id', (req,res) => {
            const id = req.params.id;
            this.most_wanted.deleteModel(id,res);
        });

        this.app.get('/most-wanted/one/:id', (req,res) => {
            const id = req.params.id;
            this.most_wanted.getMostWanted(id,res);
        });

        this.app.get('/most-wanted/alias/:id', (req,res) => {
            const id = req.params.id;
            this.most_wanted.getAllAias(id,res);
        });
        //Publish Media
        this.app.post('/production/most-wanted', (req, res) => {
            this.most_wanted.Production(req, res);
        });
        this.app.get('/production/most-wanted', (req,res) => {
            this.most_wanted.getAllWantedProduction(res);
        });

        this.app.get('/most-wanted/:page', (req,res) => {
            const page = req.params.page;
            if (!page) {
                page = 1;
            }
            this.most_wanted.getModelsPagination(page,res);
        });
    }
    routesConfig() {
        this.appRoutes();
    }

}

module.exports = routesMostWanted;