const jwt = require('jwt-simple');
//moment manejador de token
const moment = require('moment');
const secret = require('../utils/Config').server.secretJWT;

module.exports = (user) => {
    var payload = {
        sub: user.id,
        name: user.Name,
        surname: user.Surname,
        email: user.Email,
        // role: user.role,
        image: user.avatar,
        iat: moment().unix(),
        exp: moment().add(1, 'hour').unix()
    }

    return jwt.encode(payload, secret);
};