const jwt = require('jwt-simple');
//moment manejador de token
const moment = require('moment');
const secret = require('../utils/Config').server.secretJWT;
module.exports.ensureAuth = (req, res, next) => {
    if (!req.headers.authorization) {
        return reshelp(403, false, res);
    }
    var token = req.headers.authorization.replace(/['"]+/g, '');

    try {
        var payload = jwt.decode(token, secret);
        
        if (payload.exp <= moment().unix()) {
            return reshelp(401, false, res);
        }
    } catch (ex) {

        if (ex === "Error: Token expired")
            return reshelp(400, false, res);
        else
            return reshelp(500, false, res);
    }

    req.user = payload;
    next();
};

function reshelp(status, message, res) {
    res.status(status).send({ isCorrect: message });
}